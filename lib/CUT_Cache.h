//-*-c++-*-
#ifndef _CUT_Cache_h_
#define _CUT_Cache_h_
#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <values.h>
#include <memory.h>

//
// The generic cache interface
//

#ifndef CACHE_TYPE_THRESHOLD
# define CACHE_TYPE_THRESHOLD 4
#endif

enum CacheType {CacheLowAssoc, CacheHighAssoc};

template <class tagtype, int cache_type = CacheHighAssoc>
class Cache {

public:
  CacheDefs& _cd;

  //
  // Used for LRU replacement aging. You shouldn't use this to
  // determine the total number of references to the cache,
  // use the references parameters
  //
  unsigned long age_references;

  //
  // Each cache entry records the following
  //
  struct PerEntry {
    PerEntry() {
      age = 0;
      dirty = 0;
      _valid = 0;
      next = prev = hash_next = hash_prev = 0;
    }
    //
    // An age of zero means it's not valid
    //
    //
    // dirty indicates it was written to..
    //
    char dirty;
    char _valid;
    int valid() { return _valid; }
    void make_valid() { _valid = 1; }
    void make_invalid() { _valid = 0; }

    unsigned long age;
    tagtype tags;

    //
    // Information to maintain LRU order for high-associativity case
    //

    int next;
    int prev;

    void unlink(PerEntry* entries) {
      entries[next].prev = prev;
      entries[prev].next = next;
    }

    void link_before(PerEntry* entries, int me, int before) {
      prev = entries[before].prev;
      next = before;
      entries[before].prev = me;
      entries[prev].next = me;
    }

    //
    // Information to maintain HASH order for high-associativity case
    //
    int hash_next;
    int hash_prev;

    void hash_unlink(PerEntry* entries) {
      entries[hash_next].hash_prev = hash_prev;
      entries[hash_prev].hash_next = hash_next;
    }

    void hash_link_before(PerEntry* entries, int me, int before) {
      hash_prev = entries[before].hash_prev;
      hash_next = before;
      entries[before].hash_prev = me;
      entries[hash_prev].hash_next = me;
    }

    void print(int me, ostream& out) {
      out << "[ " << me << ": c = " << tags.tag() << ", next = " << next;
      out << ", prev = " << prev << ", hash_next = " << hash_next;
      out << ", hash_prev = " << hash_prev << ", valid = " << valid() << "]" << endl;
    }

  };

  //
  // The structure of the cache. This defines the information
  // and statistics get for each set
  //

  struct PerSet {
    long stores;
    long loads;
    long store_misses;
    long load_misses;
    long write_backs;

    PerSet() {
      stores = 0;
      loads = 0;
      store_misses = 0;
      load_misses = 0;
      write_backs = 0;
    }

    //
    // This is allocated and initialized when the cache is created.
    // Only one entry is used, either for low-associative or
    // high assocative caches
    //
    PerEntry *entries;
    //
    // This information is needed for the "high associtativity" caches
    //
    int lru_root;

    //
    // Output information concerning a single set/line
    //
    void output(ostream& out, int associativity) {
      out << "line at address " << hex << this << dec << " ";
      out << "[s = " << stores << ", l = " << loads << ", sm = " << store_misses;
      out << ", lm = " << load_misses << ", wb = " << write_backs << ", entries = <";
      for (int a = 0; a < associativity; a++) {
	out << " <" << entries[a].tags << ", age = " << entries[a].age;
	if (entries[a].dirty) {
	  out << ", dirty > ";
	} else {
	  out << ", clean > ";
	}
      }
      out << ">" << endl;
    }

    void print_lru_chain(PerEntry* storage, int start, char *prefix) {
      int here = start;
      do {
	PerEntry& h = storage[here];
	cout << prefix; h.print(here, cout);
	here = h.next;
      } while( here != start );
    }

    void print_hash_chain(PerEntry* storage, int start, char *prefix) {
      int here = start;
      if ( here == -1 ) {
	cout << prefix << " [hash table empty]" << endl;
	return;
      }
      do {
	PerEntry& h = storage[here];
	cout << prefix; h.print(here, cout);
	here = h.hash_next;
      } while( here != start );
    }

  };

  //
  // Information stores about each line
  //
  PerSet *lines;
  //
  // Storage array of all the entries
  //
  PerEntry *storage;

  /////////////////////////////////////////////////////////////////////////////
  // Hash table information, used for the high-associativity case
  /////////////////////////////////////////////////////////////////////////////
  static const int hashsize = 512;
  static const int hashmask = hashsize - 1;
  static const int hashtable_null = -1;
  int hashtable[hashsize];

  int gethash(tagtype& what) {
    int h =  what.tag() & hashmask;
    return h;
  }


public:

  CacheDefs& cd() {return _cd; }

public:

  //
  // Initialize the cache
  //
  Cache(CacheDefs& cd) : _cd(cd) {

    lines = new PerSet[cd.sets()];
    storage = new PerEntry[cd.blocks_total()];

    assert(cache_type == CacheLowAssoc || cache_type == CacheHighAssoc);

    if ( cache_type == CacheLowAssoc ) {
      age_references = 0;
      //
      // Make each line point to it's part of the
      // entries array;
      //
      int s = 0;
      for (int i = 0 ; i < _cd.sets(); i++ ) {
	lines[i].entries = &(storage[s]);
	s += _cd.assoc();
      }
      assert( s == cd.blocks_total() );
    } else {
      //
      // Initialize the hash table
      //
      for (int t = 0 ; t < hashsize; t++ ) {
	hashtable[t] = hashtable_null;
      }

      int s = 0;
      for (int i = 0 ; i < _cd.sets(); i++ ) {
	lines[i].lru_root = s;
	storage[s].next = s;
	storage[s].prev = s;
	storage[s].make_invalid();
	s++;
	for (int a = 1; a < _cd.assoc(); a++) {
	  storage[s].link_before(storage, s, lines[i].lru_root);
	  storage[s].make_invalid();
	  s++;
	}
      }
      assert( s == cd.blocks_total() );
    }
  }

  //
  // return TRUE if found in the list, FALSE otherwise
  // As a side effect, move the referenced item to the front
  // of the list
  //

  bool high_find_and_move_to_front(PerSet& line, tagtype& what, PerEntry*& ret_entry) {
    int hash = gethash(what);
    int start = hashtable[hash];

    if (start == hashtable_null ) {
      ret_entry = &(storage[storage[line.lru_root].prev]);
      return false;
    } else {
      int here = start;
      int times = 0;
      do {
	PerEntry& h = storage[here];

	assert( gethash(h.tags) == hash );
	assert( h.valid() );
	times++;

	if ( what(h.tags, what) ) {
	  if ( here != line.lru_root ) {
	    //
	    // Update LRU order
	    //
	    storage[here].unlink(storage);
	    storage[here].link_before(storage, here, line.lru_root);
	    line.lru_root = here;
	  }
	  //
	  // Move it to the front of the hash table to speed future
	  // lookups
	  //
	  hashtable[hash] = here;
	  ret_entry = &(storage[here]);
	  return true;
	}
	here = storage[here].hash_next;
      } while (here != start);
      //
      // Return the item that would be killed in LRU fill
      // 
      ret_entry = &(storage[storage[line.lru_root].prev]);
      return false;
    }
  }

  //
  // LRU replacement
  //
  PerEntry* high_lru_replacement(PerSet& line, tagtype& what) {
    //
    // The item at lru_root is the least referenced
    // item, and will be allocated for the new item.
    //

    //
    // Check if LRU item is already on a hash list...
    //
    int lru = storage[line.lru_root].prev;
    PerEntry& lr = storage[lru];

    if ( lr.valid() ) {
      int bucket = gethash(lr.tags);
      //
      // If this is the only thing in the hash clear the hash
      //
      if ( lr.hash_next == lru ) {
	assert(lr.hash_prev == lru);
	hashtable[bucket] = hashtable_null;
      } else {
	if ( hashtable[bucket] == lru ) {
	  hashtable[bucket] = lr.hash_next;
	}
	lr.hash_unlink(storage);
      }
    }

    int hash = gethash(what);
    lr.tags = what;
    lr.make_valid();
    line.lru_root = lru;
    //
    // Now, place it on the proper chain
    //
    if ( hashtable[hash] == hashtable_null ) {
      lr.hash_next = lr.hash_prev = lru;
    } else {
      lr.hash_link_before(storage, lru, hashtable[hash]);
    }

    hashtable[hash] = lru;
    return &lr;
  }

  /////////////////////////////////////////////////////////////////////////////
  //
  // check_for_hit
  //
  // Return TRUE if the item DOES hit in the cache.
  //
  // If TRUE, then ret_entry points to the PerEntry of the reference.
  // If FALSE, then ret_entry points to the PerEntry of the LRU item
  // that will be replaced by 'lru_fill'.
  //
  // This has the side-effect of updating the LRU status
  // of the referenced item.
  //
  bool check_for_hit(tagtype& addr, PerSet& line, PerEntry*& ret_entry) {
    if (cache_type == CacheLowAssoc) {
      //
      // Reference a given cache line, updating the age
      //
      age_references++;
      int oldest = -1;
      unsigned long oldest_age = MAXLONG;

      for (int a = 0; a < _cd.assoc(); a++ ) {
	PerEntry& entry = line.entries[a];
	if ( entry.age > 0 ) {                        // valid?
	  if ( addr(entry.tags,addr) ) {              // hit?
	    entry.age = age_references;
	    ret_entry = &entry;
	    return true;
	  }
	  if (entry.age < oldest_age ) {             // maintain for
	    oldest = a;                              // lru update
	    oldest_age = entry.age;
	  }
	} else {
	  //
	  // If this line hasn't been used, we'll fill it before
	  // anything else, but we need to keep looking for other
	  // references
	  //
	  oldest = a;
	  oldest_age = 0;
	}
      }
      ret_entry = &(line.entries[oldest]);
      return false;
    } else {
      return high_find_and_move_to_front(line, addr, ret_entry);
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  // lru_fill - place the address into the LRU item of the given
  // cache line
  /////////////////////////////////////////////////////////////////////////////

  PerEntry* lru_fill(PerSet& line, PerEntry* entry, tagtype& addr) {
    if (cache_type == CacheLowAssoc) {
      entry -> tags = addr;
      entry -> age = age_references++;
      return entry;
    } else {
      return high_lru_replacement(line, addr);
    }
  }

  //
  // Return true if it's a hit
  //
  bool load(tagtype& addr, int access_size = 8) {
    int l = addr.get_set_index(_cd);
    PerSet& line = lines[l];
    line.loads++;
    PerEntry *entry;
    if ( check_for_hit( addr, line, entry) ) {
      entry -> tags.load(_cd, addr, access_size);
      return true;
    } else {
      //
      // 'entry' points to the LRU item to replace
      //
      line.load_misses ++;
      entry -> tags.writeback(_cd, addr, access_size);

      entry = lru_fill(line, entry, addr);
      if ( entry -> dirty ) {
	line.write_backs++;
      }
      entry -> tags.load(_cd, addr, access_size);
      entry -> dirty = 0;
      return false;
    }
  }

  bool store_allocate(tagtype& addr, int access_size = 8) {
    int l = addr.get_set_index(_cd);
    PerSet& line = lines[l];

    line.stores++;

    PerEntry *entry;
    if ( check_for_hit( addr, line, entry) ) {
      entry -> tags.store(_cd, addr, access_size);
      entry -> dirty = 1;
      return true;
    } else {
      line.store_misses ++;
      entry -> tags.writeback(_cd, addr, access_size);
      entry = lru_fill(line, entry, addr);
      if ( entry -> dirty ) {
	line.write_backs++;
      }
      entry -> tags.store(_cd, addr, access_size);
      entry -> dirty = 1;
      return false;
    }
  }

  bool store_noallocate(tagtype& addr, int access_size = 8) {
    int l = addr.get_set_index(_cd);
    assert(l >= 0);
    assert(l < _cd.sets());

    PerSet& line = lines[l];

    line.stores++;
    PerEntry *entry;
    if ( check_for_hit( addr, line, entry) ) {
      entry -> tags.store(_cd, addr, access_size);
      entry -> dirty = 1;
      return true;
    } else {
      line.store_misses ++;
      return false;
    }
  }

  /////////////////////////////////////////////////////////////////////////////
  //
  // Return individual statistics
  //
  /////////////////////////////////////////////////////////////////////////////
  long stores() {
    long foo = 0;
    for (long i=0; i < _cd.sets(); i++) {
      foo += lines[i].stores;
    }
    return foo;
  }

  long loads() {
    long foo = 0;
    for (long i=0; i < _cd.sets(); i++) {
      foo += lines[i].loads;
    }
    return foo;
  }
  long store_misses() {
    long foo = 0;
    for (long i=0; i < _cd.sets(); i++) {
      foo += lines[i].store_misses;
    }
    return foo;
  }
  long load_misses() {
    long foo = 0;
    for (long i=0; i < _cd.sets(); i++) {
      foo += lines[i].load_misses;
    }
    return foo;
  }

  long write_backs() {
    long foo = 0;
    for (long i=0; i < _cd.sets(); i++) {
      foo += lines[i].write_backs;
    }
    return foo;
  }

  //
  // Output routines
  //

  void put_header(const char *prefix, ostream& output) {
    output << prefix << '_';
  }

  void short_output(ostream& output) {
    const char *prefix = _cd.name();

    _cd.output(output);

    if (cache_type == CacheLowAssoc) {
      put_header(prefix,output);
      output << "type = low_assoc\n";
    } else {
      put_header(prefix,output);
      output << "type = high_assoc\n";
    }

    unsigned long totals = loads() + stores();
    unsigned long total_misses = load_misses() + store_misses();

    put_header(prefix, output);
    output << "load_references  -> " << loads() << "\n";

    put_header(prefix, output);
    output << "load_misses -> " << load_misses() << "\n";
    double load_miss_rate = load_misses();
    if ( loads() > 0 ) {
      load_miss_rate /= loads();
    } else {
      load_miss_rate = 0.0;
    }
    load_miss_rate *= 100.0;

    put_header(prefix, output);
    output << "load_miss_rate -> " << load_miss_rate << "\n";

    put_header(prefix, output);
    output << "store_references -> " << stores() << "\n";
    put_header(prefix, output);
    output << "store_misses     -> " << store_misses() << "\n";
    double store_miss_rate = store_misses();
    if ( stores() > 0 ) {
      store_miss_rate /= stores();
    } else {
      store_miss_rate = 0.0;
    }
    store_miss_rate *= 100.0;
    put_header(prefix, output);
    output << "store_miss_rate  -> " << store_miss_rate << "\n";

    put_header(prefix, output);
    output << "total_references -> " << totals << "\n";
    put_header(prefix, output);
    output << "total_misses     -> " << total_misses << "\n";
    double total_miss_rate = total_misses;
    if ( totals > 0 ) {
      total_miss_rate /= totals;
    } else {
      total_miss_rate = 0.0;
    }
    total_miss_rate *= 100.0;
    put_header(prefix, output);
    output << "total_miss_rate  -> " << total_miss_rate << "\n";

    put_header(prefix, output);
    output << "write_backs      -> " << write_backs() << "\n";
  }


};

#endif
