#ifndef _CUT_CacheDefs_h_
#define _CUT_CacheDefs_h_

#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

extern "C" double log2(double);

//
// Unified cache data structure
//

//
// Definitions needed across all cache implementations.
//
// Here's the terminology we use:
//
// A cache is composed of sets.
// For an n-assoc cache, each set contains n blocks.
//
// The total size is sets * assoc * blocksize.
//
// The restrictions that we make for efficiency are that the block
// size and the number of sets are a power of two. The associativity
// can be any value.
//

class CacheDefs {
public:
  typedef unsigned long address;

  CacheDefs(char *name, int total_size_in_bytes, int block_size, int assoc) {
    _name = name;
    initialize(total_size_in_bytes, block_size, assoc);
  }

  //
  // Alternative constructor. Specify the total cache size

  CacheDefs(char *name, char *spec) {

    _name = name;

    int size;
    char size_scale[128];
    int blksz;
    char blksz_scale[128];
    int assoc;

    size_scale[0] = 0;
    blksz_scale[0] = 0;

    //
    // All suffixes
    //
    int result = sscanf(spec, "%d%[kKmM]:%d%[kKmM]:%d",
			&size, size_scale,
			&blksz, blksz_scale,
			&assoc);

    if ( result != 5 ) {
      //
      // size only suffix
      //

      result = sscanf(spec, "%d%[kKmM]:%d:%d",
		      &size, size_scale,
		      &blksz,
		      &assoc);

      if ( result == 4 ) {
	blksz_scale[0] = 0;
      } else {
	//
	// block size only suffix
	//
	result = sscanf(spec, "%d:%d%[kKmM]:%d",
			&size,
			&blksz, blksz_scale,
			&assoc);

	if ( result == 4 ) {
	  size_scale[0] = 0;
	} else {
	  result = sscanf(spec, "%d:%d:%d", &size, &blksz, &assoc);
	  if ( result != 3 ) {
	    cerr << "Improper specification for cache configuration: " << spec << endl;
	    cerr << "Only " << result << " fields found\n";
	    assert(result == 3);
	  }
	}
      }
    }

    if ( strlen(size_scale) == 0 ) {
      // no scaling
    } else if ( !strcmp(size_scale, "k") || !strcmp(size_scale, "K") ) {
      size *= 1024;
    } else if (!strcmp(size_scale, "m") || !strcmp(size_scale, "M")) {
      size *= (1024 * 1024);
    } else {
      cerr << "Improper scaling factor for cache size configuration: "
	   << size_scale << endl;
      cerr << "Cache configuration was specified as >>" << spec << "<<" << endl;
      assert("improper scaling factor");
    }

    if ( strlen(blksz_scale) == 0 ) {
      // no scaling
    } else if ( !strcmp(blksz_scale, "k") || !strcmp(blksz_scale, "K") ) {
      blksz *= 1024;
    } else if (!strcmp(blksz_scale, "m") || !strcmp(blksz_scale, "M")) {
      blksz *= (1024 * 1024);
    } else {
      cerr << "Improper scaling factor for cache block size configuration: "
	   << blksz_scale << endl;
      cerr << "Cache configuration was specified as >>" << spec << "<<" << endl;
      assert("improper scaling factor");
    }

    initialize( size, blksz, assoc);
  }

  int block_size() const { return _block_size; }

  int lg_block_size() const { return _lg_block_size; }

  int blocks_total() const { return _blocks_total; }

  int sets() const { return _sets; }

  int lg_sets() const { return _lg_sets; }

  address set_mask() const { return _set_mask; }

  int assoc() const { return _assoc; }

  long size_in_bytes() const {
    return block_size() * blocks_total();
  }
 
  const char *name() const {
    return _name;
  }

  void output(ostream& str) {
    str << _name << "_size = " << size_in_bytes() << endl;
    str << _name << "_block_size = " << block_size() << endl;
    str << _name << "_lg_block_size = " << lg_block_size() << endl;
    str << _name << "_assoc = " << assoc() << endl;
    str << _name << "_sets = " << sets() << endl;
    str << _name << "_lg_sets = " << lg_sets() << endl;
    str << _name << "_set_mask = " << set_mask() << endl;
  }

private:
  void initialize(int total_size_in_bytes, int block_size, int assoc) {
    _block_size = block_size;
    _assoc = assoc;
    //
    // derive and check for mis-specification
    //
    _blocks_total = total_size_in_bytes / _block_size;
    assert( (_blocks_total * _block_size) == total_size_in_bytes);

    _sets = _blocks_total / _assoc;
    assert( (_sets * _assoc) == _blocks_total);

    _lg_sets = (int) log2(_sets);
    assert((1 << _lg_sets) == _sets);
    
    _set_mask = ~(( (address) -1L) << _lg_sets);

    _lg_block_size = (int) log2(_block_size);
    assert((1 << _lg_block_size) == _block_size);
  }

  //
  // The size of each cache entry.
  // Must be a power of two.
  //
  int _block_size;
  int _lg_block_size;

  //
  // The total number of blocks.
  // Does not need to be a power of two.
  //
  int _blocks_total;

  //
  // The number of sets
  // Needs to be a power of two.
  //
  int _sets;
  int _lg_sets;
  address _set_mask;

  //
  // The associativity.
  // Does not need to be a power of two.
  // 
  int _assoc;
  //
  // The cache name
  //
  char *_name;
};

//
// The structure of a cache tag.
//
class TagVIVT {
public:
  //
  // We explicitly store the address and the tag
  //
  CacheDefs::address _vaddr;
  CacheDefs::address _tag;

  TagVIVT() {
    _vaddr = 0;
  }

  TagVIVT(const CacheDefs& cd, CacheDefs::address vaddr_) {
    _vaddr = vaddr_;
    _tag = _vaddr >> cd.lg_block_size();
  }

  virtual CacheDefs::address tag() {
    return _tag;
  }

  virtual CacheDefs::address get_set_index(const CacheDefs& cd) {
    return _tag & cd.set_mask();
  }

  //
  // Comparison function to determine if two cache tags match.
  //
  virtual int operator()(TagVIVT& one, TagVIVT& two) {
    return (one._tag == two._tag);
  }
  //
  // Call back functions. Used to customize aspects of
  // you cache protocols
  //
  virtual void store(CacheDefs& cd, TagVIVT& addr, int access_size)
  {
    // empty
  }

  virtual void load(CacheDefs& cd, TagVIVT& addr, int access_size)
  {
    // empty
  }

  virtual void writeback(CacheDefs& cd, TagVIVT& addr, int access_size)
  {
    // empty
  }

};


class TagVIPT : public TagVIVT {
public:

  CacheDefs::address _paddr;

  TagVIPT() {
    _tag = 0;
    _paddr = 0;
  }

  TagVIPT(const CacheDefs& cd, CacheDefs::address vaddr, CacheDefs::address paddr)
    : TagVIVT(cd, vaddr) {
      _tag = paddr >> cd.lg_block_size();
      _paddr = paddr;
  }

};

class TagVIPTpid : public TagVIPT {
public:

  TagVIPTpid() {
    _pid = 0;
  }

  TagVIPTpid(const CacheDefs& cd,
	  CacheDefs::address vaddr,
	  CacheDefs::address paddr,
	  int pid)
    : TagVIPT(cd, vaddr, paddr) {
      _pid = pid;
  }

  int pid() const { return _pid; }

  virtual int operator()(TagVIPTpid& one, TagVIPTpid& two) {
    return (one._tag == two._tag) && (one._pid == two._pid);
  }

private:

  int _pid;
};

#endif
