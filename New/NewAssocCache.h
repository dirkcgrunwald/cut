//-*-c++-*-
#ifndef _AssocCache_h_
#define _AssocCache_h_

#include <iostream.h>
#include <stdio.h>
#include <values.h>

//
// lg_lines is how many cache blocks we want to create of size
//'block_size_shift'. We compensate for the increase in
//associativity. Thus, no matter what the increase in associativity,
//the size of the cache is the same.
//
// block_size_shift is the block size in bits
// (e.g., 5 -> 32 byte lines)
//

//
// I thought this class would be faster than the
// old AssocCache, but timing reveals that it's not.
// I have no clue why.
//

template <class address, int lg_assoc, int lg_lines, int block_size_shift>
class AssocCache {
public:
#ifdef CACHE_RECORD_PERLINE
    long stores[1 << lg_lines];
    long loads[1 << lg_lines];
    long store_misses[1 << lg_lines];
    long load_misses[1 << lg_lines];
#  ifdef CACHE_RECORD_WRITEBACKS
    long write_backs[1 << lg_lines];
#  endif
#else
    long stores;
    long loads;
    long store_misses;
    long load_misses;
    long write_backs;
#endif


    address tags[1 << lg_lines][(1 << lg_assoc)];
    int valid[1 << lg_lines][(1 << lg_assoc)];
    int age[1 << lg_lines][(1 << lg_assoc)];
    int dirty[1 << lg_lines][(1 << lg_assoc)];

public:

    const int numlines() const {
	return ( 1 << lg_lines );
    }

    const int assoc() const {
	return ( 1 << lg_assoc );
    }

    AssocCache() {
	//
	// Clear everything, set age to maximum age
	//
#ifdef CACHE_RECORD_PERLINE
	memset(loads, 0, sizeof(long) * numlines());
	memset(stores, 0, sizeof(long) * numlines());
	memset(store_misses, 0, sizeof(long) * numlines());
	memset(load_misses, 0, sizeof(long) * numlines());
#  ifdef CACHE_RECORD_WRITEBACKS
	memset(write_backs, 0, sizeof(long) * numlines());
#  endif
#else
	stores = 0;
	loads = 0;
	store_misses = 0;
	load_misses = 0;
	write_backs = 0;
#endif


	memset(tags, 0, sizeof(address) * (1 << (lg_lines + lg_assoc)));
	memset(valid, 0, sizeof(int) * (1 << (lg_lines + lg_assoc)));
	memset(age, 0, sizeof(int) * (1 << (lg_lines + lg_assoc)));
	memset(dirty, 0, sizeof(int) * (1 << (lg_lines + lg_assoc)));

	for (int l = 0 ; l < numlines(); l++ ) {
	    for (int s = 0; s < assoc(); s++ ) {
		age[l][s] = MAXINT;
	    }
	}
    }
  
    unsigned int line(address addr) {
	int l = (addr >> (block_size_shift));
	l = l & (~(-1 << (lg_lines-lg_assoc)));
	return l;
    }
  
    address tag(address addr) {
	address t = (addr >> block_size_shift);
	return t;
    }

    void line_and_tag(int& l, address& t, address addr) {
	t = (addr >> block_size_shift);
	l = t & (~(-1 << (lg_lines-lg_assoc)));
    }
  
    //
    // ref_if_hit updates the age on the reference if there's a hit
    // and returns TRUE if there's a hit. It returns FALSE if there's
    // a miss and the line needs to be filled.
    //
    int ref_if_hit(address addr, int l, address t, int make_dirty) {
	//
	// Reference a given cache line, updating the age
	//
	for (int a = 0; a < assoc(); a++ ) {
	    if ( valid[l][a] & tags[l][a] == t ) {
		//
		// The following is not done for direct mapped caches.
		//
		if ( lg_assoc > 0 ) {
		    int myage = age[l][a];
		    //
		    // a is our cache line. Now, scan all other cache
		    //lines and increment their ages if they are
		    //younger.
		    //
		    for (int aa = 0; aa < assoc(); aa++ ) {
			if ( valid[l][aa] && age[l][aa] < myage ) {
			    age[l][aa] += 1;
			}
		    }
		    //
		    // Make this line be the youngest
		    //
		    age[l][a] = 0;
		}
		//
		// Checking for dirty == 1 reduces the number of
		//stores when we're not dirtying things (load)
		//
		if ( make_dirty == 1 ) {
		    dirty[l][a] = make_dirty;
		}
		return 1;
	    }
	}
	//
	// Miss
	//
	return 0;
    }
  
    void fill(address addr, int l, address t, int make_dirty) {
	if ( lg_assoc == 0 ) {
	    valid[l][0] = 1; 

#ifdef CACHE_RECORD_WRITEBACKS
#  ifdef CACHE_RECORD_PERLINE
	    if (dirty[l][0]) {
		write_backs[l]++;
	    }
#  else
	    write_backs++;
#  endif
#endif

	    tags[l][0] = t;

	    if ( make_dirty ) {
		dirty[l][0] = make_dirty;
	    }

	} else {
	    //
	    // Find the oldest existing line, but also take the first
	    //empty line.
	    //
	    int oldest = -1;
	    int oldest_age = -1;
      
	    for (int a = 0; a < assoc(); a++ ) {
		if ( valid[l][a] && age[l][a] > oldest_age ) {
		    oldest = a;
		    oldest_age = age[l][a];
		} else if ( ! valid[l][a] ) {
		    valid[l][a] = 1;
		    oldest = a;
		    break;
		}
	    }
	    //
	    // oldest is the one with the oldest age. We replace that
	    //one, but we update all ages first.
	    //
	    for (int aa = 0; aa < assoc(); aa++ ) {
		age[l][aa] += 1;
	    }
	    //
	    // Make this line be the youngest
	    //
#ifdef CACHE_RECORD_WRITEBACKS
#  ifdef CACHE_RECORD_PERLINE
	    if (dirty[l][oldest]) {
		write_backs[l]++;
	    }
#  else
	    write_backs++;
#  endif
#endif

	    tags[l][oldest] = t;
	    if ( make_dirty ) {
		dirty[l][oldest] = make_dirty;
	    }
	    age[l][oldest] = 0;
	}
    }
  
    //
    // Return 1 if it's was a miss, 0 otherwise
    void load(address addr) {
	int l;
	address t;
	line_and_tag(l, t, addr);

#ifdef CACHE_RECORD_PERLINE
	loads[l]++;
#else
	loads++;
#endif
	if ( !ref_if_hit( addr, l, t, 0 ) ) {
#ifdef CACHE_RECORD_PERLINE
	    load_misses[l]++;
#else
	    load_misses++;
#endif
	    fill(addr, l, t, 0);
	}
    }

    //
    // This makes 'references' references to the SAME cache line
    // located at address 'addr'. This is mainly used for faster
    // instruction cache simulation.
    //
    void load_same_line(address addr, int references) {
	int l;
	address t;
	line_and_tag(l, t, addr);

#ifdef CACHE_RECORD_PERLINE
	loads[l] += references;
#else
	loads += references;
#endif
	if ( !ref_if_hit( addr, l, t, 0 ) ) {
	    //
	    // A miss counts for one miss, no matter how many loads
	    //there are to it.
	    //
#ifdef CACHE_RECORD_PERLINE
	    load_misses[l]++;
#else
	    load_misses++;
#endif
	    fill(addr, l, t, 0);
	}
    }

    void store_allocate(address addr) {
	int l;
	address t;
	line_and_tag(l, t, addr);

#ifdef CACHE_RECORD_PERLINE
	stores[l]++;
#else
	stores++;
#endif
	if ( !ref_if_hit( addr, l, t, 1 ) ) {
#ifdef CACHE_RECORD_PERLINE
	    store_misses[l]++;
#else
	    store_misses++;
#endif
	    fill(addr, l, t, 1);
	} 
    }

    void store_no_allocate(address addr) {
	int l;
	address t;
	line_and_tag(l, t, addr);

#ifdef CACHE_RECORD_PERLINE
	stores[l]++;
#else
	stores++;
#endif
	if ( !ref_if_hit( addr, l, t, 1) ) {
#ifdef CACHE_RECORD_PERLINE
	    store_misses[l]++;
#else
	    store_misses++;
#endif
	}
    }

    int flush_dirty() {
#ifdef CACHE_RECORD_WRITEBACKS
	int total_write_backs = 0;
	for (int l = 0 ; l < numlines(); l++ ) {
	    for (int a = 0 ; a < assoc(); a++ ) {
		if ( dirty[l][a] ) {
		    write_backs[l]++;
		    total_write_backs++;
		    dirty[l][a] = 0;
		}
	    }
	}
	return total_write_backs;
#else
	return 0;
#endif
    }

    //
    // Only changed if you use for_each_reference
    //
    long num_stores() {
#ifdef CACHE_RECORD_PERLINE

	long foo = 0;
	for (long i=0; i < numlines(); i++) {
	    foo += stores[i];
	}
	return foo;
#else
	return stores;
#endif
    }

    long num_loads() {
#ifdef CACHE_RECORD_PERLINE
	long foo = 0;
	for (long i=0; i < numlines(); i++) {
	    foo += loads[i];
	}
	return foo;
#else
	return loads;
#endif
    }
    long num_store_misses() {
#ifdef CACHE_RECORD_PERLINE
	long foo = 0;
	for (long i=0; i < numlines(); i++) {
	    foo += store_misses[i];
	}
	return foo;
#else
	return store_misses;
#endif
    }
    long num_load_misses() {
#ifdef CACHE_RECORD_PERLINE
	long foo = 0;
	for (long i=0; i < numlines(); i++) {
	    foo += load_misses[i];
	}
	return foo;
#else
	return load_misses;
#endif
    }


#ifdef CACHE_RECORD_WRITEBACKS
    long num_write_backs() {
	long foo = 0;
	for (long i=0; i < numlines(); i++) {
	    foo += write_backs[i];
	}
	return foo;
    }
#endif

    //
    // Statistics reporting
    //


    void fini(ostream& output) {
	output << "-----------------------------------";
	output << "-----------------------------------\n";
	output << "  ";
	output << assoc() << "-way Assoc Cache, ";
	output << (1 << block_size_shift) << " byte lines, ";
	output << numlines() << " lines, ";
	int size =(1 << (block_size_shift + lg_lines + lg_assoc));
	size /= 1024;
	output << size << "K bytes\n";
	output << "-----------------------------------";
	output << "-----------------------------------\n";


	int totals = num_loads() + num_stores();
	int total_misses = num_load_misses() + num_store_misses();

	output << "Load references  = " << num_loads() << "\n";
	output << "Load misses      = " << num_load_misses() << "\n";
	double load_miss_rate = num_load_misses();
	if ( num_loads() > 0 ) {
	    load_miss_rate /= num_loads();
	} else {
	    load_miss_rate = 0.0;
	}
	load_miss_rate *= 100.0;
	output << "Load miss rate   = " << load_miss_rate << "%\n";
	output << "\n";

	output << "Store references = " << num_stores() << "\n";
	output << "Store misses     = " << num_store_misses() << "\n";
	double store_miss_rate = num_store_misses();
	if ( num_stores() > 0 ) {
	    store_miss_rate /= num_stores();
	} else {
	    store_miss_rate = 0.0;
	}
	store_miss_rate *= 100.0;
	output << "Store miss rate  = " << store_miss_rate << "%\n";
	output << "\n";

	output << "Total references = " << totals << "\n";
	output << "Total misses     = " << total_misses << "\n";
	double total_miss_rate = total_misses;
	if ( totals > 0 ) {
	    total_miss_rate /= totals;
	} else {
	    total_miss_rate = 0.0;
	}
	total_miss_rate *= 100.0;
	output << "Total miss rate  = " << total_miss_rate << "%\n";
	output << "\n";


#ifdef CACHE_RECORD_WRITEBACKS
	output << "Write backs      = " << num_write_backs() << "\n";
#else
	output << "Write backs not recorded\n";
#endif
    }

    void put_header(char *prefix, ostream& output) {
	int size =(1 << (block_size_shift + lg_lines + lg_assoc));
	output << prefix << '_';
    }


    void digestable_report(ostream& output, char *prefix) {
	int block_size = (1 << block_size_shift);
	int size = block_size * numlines() * assoc();
	put_header(prefix, output);
	output << "block_size -> " << block_size << "\n";
	put_header(prefix, output);
	output << "blocks -> " << numlines() << "\n";
	put_header(prefix, output);
	output << "assoc -> " << assoc() << "\n";
	put_header(prefix, output);
	output << "size -> " << size << "\n";

	unsigned long totals = num_loads() + num_stores();
	unsigned long total_misses = num_load_misses() + num_store_misses();

	put_header(prefix, output);
	output << "load_references  -> " << num_loads() << "\n";

	put_header(prefix, output);
	output << "load_misses -> " << num_load_misses() << "\n";
	double load_miss_rate = num_load_misses();
	if ( num_loads() > 0 ) {
	    load_miss_rate /= num_loads();
	} else {
	    load_miss_rate = 0.0;
	}
	load_miss_rate *= 100.0;

	put_header(prefix, output);
	output << "load_miss_rate -> " << load_miss_rate << "\n";

	put_header(prefix, output);
	output << "store_references -> " << num_stores() << "\n";
	put_header(prefix, output);
	output << "store_misses     -> " << num_store_misses() << "\n";
	double store_miss_rate = num_store_misses();
	if ( num_stores() > 0 ) {
	    store_miss_rate /= num_stores();
	} else {
	    store_miss_rate = 0.0;
	}
	store_miss_rate *= 100.0;
	put_header(prefix, output);
	output << "store_miss_rate  -> " << store_miss_rate << "\n";

	put_header(prefix, output);
	output << "total_references -> " << totals << "\n";
	put_header(prefix, output);
	output << "total_misses     -> " << total_misses << "\n";
	double total_miss_rate = total_misses;
	if ( totals > 0 ) {
	    total_miss_rate /= totals;
	} else {
	    total_miss_rate = 0.0;
	}
	total_miss_rate *= 100.0;
	put_header(prefix, output);
	output << "total_miss_rate  -> " << total_miss_rate << "\n";

	put_header(prefix, output);
#ifdef CACHE_RECORD_WRITEBACKS
	output << "write_backs      -> " << num_write_backs() << "\n";
#else
	output << "Write backs not recorded\n";
#endif

    }

};

#endif
