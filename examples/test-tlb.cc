//
// This is the same as "test-cachetags", but is customized to behave
// like a TLB rather than a cache
//
//#define CACHE_TYPE_THRESHOLD 1024

#include "CUT_CacheDefs.h"
#include "CUT_Cache.h"
#include "CUT_Histogram.h"
#include "rpcc.h"

const int howmany = 1000000;
//
// Assume 8KB pages
// assume 64 entry, full associative.
// Mapping space is thus 8KB * 64, or 524288
//
char *spec = "512K:8K:64";

void test_vivt()
{
  //
  // For a TLB and a single process, it doesn't really matter if the
  // tag is a VIVT or a VIPT.
  //
  CacheDefs cd("low_vipt", spec);
  typedef Cache<TagVIVT> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();

  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0x7fffff;
    TagVIVT tag(cd, addr);

    if (random() & 0x1) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";

  cache.short_output(cout);
}

int
main()
{
  srandom(0);
  test_vivt();
  return 0;
}
