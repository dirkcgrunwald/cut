//-*-c++-*-
#ifndef _CUT_RegressionData_h_
#define _CUT_RegressionData_h_

#include "CUT_Statistic.h"

/**
 * A CUT_RegressionData collects data that is then
 * used to derive a CUT_RegressionModel. You can continue
 * to collect data once it's been turned into a CUT_RegressionModel
 * without influencing the model.
 *
 */

template <class T>
class CUT_RegressionData {

public:
  CUT_RegressionData() {
    reset();
  }

  /**
   * You can reset the CUT_Statistic, meaning that all previously
   * recorded data is discarded.
   */
  void reset() {
    x.reset();
    y.reset();
    sum_x_y = 0;
  }

  /**
   * Add a sample
   */
  void add(double xx, double yy) {
    x.add(xx);
    y.add(yy);
    sum_x_y += (xx * yy);
  }

  /**
   * Data needed by the CUT_Regression model. We use two
   * CUT_Statistics to record most of the data.
   */
  CUT_Statistic<T> x;
  CUT_Statistic<T> y;
  T sum_x_y;
};

#endif
