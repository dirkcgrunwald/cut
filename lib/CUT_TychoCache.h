//-*-c++-*-
#ifndef _CUT_TychoCache_h_
#define _CUT_TychoCache_h_

#ifndef MAX_LGNUMSETS
#  define MAX_LGNUMSETS 32
#endif

#ifndef MAX_ASSOC
#  define MAX_ASSOC 32
#endif

#ifndef NULL
# define NULL ((void *) 0)
#endif


#include <iostream>
#include <ostream>

using namespace std;

class TychoStackNode;

typedef unsigned long TychoTag;
typedef unsigned long TychoAddr;

class CUT_TychoCache { 

  public:
    CUT_TychoCache( const int _blksize = 32,
		    const int _min_lns = 7,
		    const int _max_lns = 10,
		    const int _max_assoc = 8
		    );

    void for_each_reference(TychoAddr address, int references = 1);

    void flushcache();
    void fini(ostream&);

    void outputmetrics(ostream&);
    void outputmissratios(ostream&);
    void outputmissdata(ostream&, char *title);

    enum IsValid {InValid, Valid };

  protected:
    //
    // CacheType
    //
    unsigned int blocksize;
    unsigned int blockshift;

    int min_lgnumsets;
    int max_lgnumsets;
    int diff_lgnumsets;
    int min_set_mask;
    unsigned int num_stacks;
    int max_assoc;
    int max_lgassoc;

    //
    // Metrics
    //
    unsigned long distance[MAX_LGNUMSETS+1][MAX_ASSOC+1];
    // Used to accumulate distance counts that 
    // overflow the 32b integers of distance 
    double distance_double[MAX_LGNUMSETS+1][MAX_ASSOC+1];

    int *clean_up_counters;
    // counters for reclaiming stack nodes/


    unsigned long num_references;
    double num_references_double;
    int max_assoc_encountered;
    int total_stack_depth;
    double total_stack_depth_double;
    
    TychoStackNode *stack;

    //
    // Function interfaces
    //
    TychoTag movetotop(TychoStackNode *preptr,
		       TychoStackNode *pteptr,
		       int stacknum);

    int push(TychoStackNode*, int stacknum);

    TychoStackNode * update_stack(int not_found, int stack_num,
				  TychoStackNode *ptr, TychoStackNode *preptr,
				  IsValid valid_bit, TychoTag addr_tag);

    void update_distances(const int *above,
			  const int not_found,
			  const int stackdepth,
			  int refs_this_line );

    int match(TychoTag tag1, TychoTag tag2, int diff_lg_sets);
    
    int huh_remove(TychoStackNode *preptr, TychoStackNode *ptr, int stacknum);
    void clean_up_stacks();
    void re_initmetric();
    void checkintegrity(const int assoc_limit);
    void checkintegrity_double(const int assoc_limit);
    void accumulate_metric();

    int print_cachesize(ostream&, int cachesize);

};

#endif
