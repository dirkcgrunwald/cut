#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

/////////////////////////////////////////////////////////////////////////////
// This is a test of the CUT_PhysToMemSet object. This object is used
// to define "regions" of memory and map them to "objects". We then define
// a set<> type to contain these regions.
//
// This is useful anytime you want to provide "callbacks" to
// different regions of memory. This example provides to "kinds"
// of memory -- RangeItem is a "memory range" that provides a store
// for a region of memory. RangeIO is intended to emulate I/O devices.
// The basic gist of this example is that you can have different classes
// or memory regions, search for them and have callbacks from them.
//
/////////////////////////////////////////////////////////////////////////////

#include "CUT_PhysToMemSet.h"

typedef CUT_PhysToMemObj<uint32_t> RangeRoot;

/////////////////////////////////////////////////////////////////////////////
// Something that represents memory
/////////////////////////////////////////////////////////////////////////////

class RangeItem : public RangeRoot {
  uint8_t *storage;
  char *message_;
public:

  RangeItem(int low, int high, char *message = "<default>")
    : RangeRoot(low, high)
  {
    message_ = message;
    storage = new uint8_t[high-low+1];
  }

  char * message() { return message_; }

  virtual uint8_t read_byte(uint32_t addr)
  {
    uint8_t *p = (storage + (addr - low_));
    cout << "Read " << (int) *p << " from " << addr << endl;
    return *p;
  }

  virtual void write_byte(uint32_t addr, uint8_t value)
  {
    uint8_t *p = (storage + (addr - low_));
    *p = value;
    cout << "Write " << (int) *p << " to " << addr << endl;
  }
};

/////////////////////////////////////////////////////////////////////////////
// Something that an I/O register
/////////////////////////////////////////////////////////////////////////////

class RangeIO : public  RangeItem {
public:
  RangeIO(int low, int high, char *message) : RangeItem(low, high, message)
  {
  }

  virtual uint8_t read_byte(uint32_t addr) {
    cout << "Read IO from " << addr << endl;
    return 0;
  }

  virtual void write_byte(uint32_t addr, uint8_t value) {
    cout << "Write IO " << (int) value << " to " << addr << endl;
  }
};

typedef set<RangeItem*, less_ptr<RangeItem> > RangeSet;

int
main(int argc, char **argv)
{
  RangeSet myset;

  /////////////////////////////////////////////////////////////////////////////
  // Add some specific memory map items..
  /////////////////////////////////////////////////////////////////////////////

  myset.insert( new RangeIO(1,19,"Serial register") );
  myset.insert( new RangeIO(20,149, "DMA space") );
  myset.insert( new RangeIO(150,199, "DMA2 space") );
  myset.insert( new RangeIO(200,200, "Very small memory") );
  myset.insert( new RangeIO(250,399, "Some IO Device") );
  myset.insert( new RangeIO(400,499, "Another Small memory") );

  for(int i = 500; i < 1000; i+=100) {
    char buffer[1024];
    sprintf(buffer,"This is memory block #%d", i);
    RangeItem *a = new RangeItem(i, i+99, strdup(buffer));
    myset.insert(a);
  }

  /////////////////////////////////////////////////////////////////////////////
  // Now, dump them all out....
  /////////////////////////////////////////////////////////////////////////////

  cout << "Here's the memory map..\n";
  for(RangeSet::iterator i = myset.begin();
      i != myset.end();
      i++) {
    RangeItem *item = *i;
    cout << "item is from " << item -> low_ << " to " << item -> high_;
    cout << ", also known as " << item -> message() << endl;
  }

  //
  // Now do something like a simulator would do...
  //
  for (int i = 0; i < 10; i++) {
    long int i = random() % 1500;
    RangeItem point(i,i,NULL);

    RangeSet::iterator j = myset.find(&point);
    
    if (j == myset.end()) {
      cout << "Probe with i = " << i << " failed "<< endl;
    } else {
      int rw = random() & 0x1;
      RangeItem *item = *j;
      if (rw) {
	item -> read_byte(i);
      } else {
	item -> write_byte(i,i);
      }
    }
  }
}
