//-*-c++-*-
#ifndef _CUT_AssocCache_h_
#define _CUT_AssocCache_h_

#include <iostream.h>
#include <stdio.h>
#include <values.h>
#include <memory.h>

template <class address, int lg_assoc, int block_size_shift>
class CUT_AssocCache {
public:
    int lglines;
    int lgsets;
    int numlines;

    unsigned long lgsets_mask;
  
    struct PerSet {
	long stores;		// 8
	long loads;		// 8
	long store_misses;	// 8
	long load_misses;	// 8
	long write_backs;	// 8

	address tags[(1 << lg_assoc)];	// N*8
	int valid[(1 << lg_assoc)];	// N*4
	int age[(1 << lg_assoc)];	// N*4
	int dirty[(1 << lg_assoc)];	// N*4
	//
	// In a direct mapped cache, N = 1 and storage is
	// 54 bytes. The compiler rounds this to 56 bytes
	// for long-word alignment.
	// We need to add padding to round this out to a POW2.
	// For N=1, storage needs are : 48 +  6 + 2 = 56
	// For N=2, storage needs are : 48 + 28 + 4 = 80
    };

    PerSet *lines;

public:
  
    //
    // lg_lines is how many cache blocks we want to create of size
    // 'block_size_shift'. We compensate for the increase in
    // associativity. Thus, no matter what the increase in
    // associativity, the size of th cache is the same.
    //
    // block_size_shift is the block size in bits (e.g., 5 -> 32 byte lines)
    //
    CUT_AssocCache(int lg_lines) {
	lglines = lg_lines;
	lgsets = lglines - lg_assoc;
	lgsets_mask = ~(-1 << lgsets);
      
	numlines = 1 << lgsets;
      
	lines = new PerSet[ numlines ];
	memset(lines, 0, sizeof(PerSet) * numlines);
	for (int i = 0 ; i < numlines; i++ ) {
	    for (int s = 0; s < (1 << lg_assoc); s++ ) {
		lines[i].age[s] = MAXSHORT;
	    }
	}
    }
  
    unsigned int line(address addr) {
	int l = (addr >> (block_size_shift));
	l = l & lgsets_mask;
	return l;
    }
  
    address tag(address addr) {
	address t = (addr >> block_size_shift);
	return t;
    }

    void line_and_tag(int& l, address& t, address addr) {
	t = (addr >> block_size_shift);
	l = t & lgsets_mask;
    }
  
    int is_miss(address addr) {
	int l = line(addr);
	address t = tag(addr);
	int found = 0;
	for (int a = 0; a < (1 << lg_assoc); a++ ) {
	    if ( lines[l].valid[a] & lines[l].tags[a] == t ) {
		return 0;
	    }
	}
	return ( 1 );
    };
  
    int is_hit(address addr) {
	return ( ! is_miss(addr) );
    }
  
    //
    // ref_if_hit updates the age on the reference if there's a hit
    // and returns TRUE if there's a hit. It returns FALSE if there's
    // a miss and the line needs to be filled.
    //
    int ref_if_hit(address addr, PerSet*line,
		   int l, address t, int make_dirty) {
	//
	// Reference a given cache line, updating the age
	//
	for (int a = 0; a < (1 << lg_assoc); a++ ) {
	    if ( line -> valid[a] & line -> tags[a] == t ) {
		if ( lg_assoc > 0 ) {
		    int myage = line -> age[a];
		    //
		    // a is our cache line. Now, scan all other cache lines
		    // and increment their ages if they are younger.
		    //
		    for (int aa = 0; aa < (1 << lg_assoc); aa++ ) {
			if ( line -> valid[aa] & line -> age[aa] < myage ) {
			    line -> age[aa] += 1;
			}
		    }
		    //
		    // Make this line be the youngest
		    //
		    line -> age[a] = 0;
		}
		if ( make_dirty == 1 ) {
		    line -> dirty[a] = make_dirty;
		}
		return 1;
	    }
	}
	//
	// Miss
	//
	return 0;
    }
  
    void fill(address addr, PerSet* line, int l, address t, int make_dirty) {
	if ( lg_assoc == 0 ) {
	    line -> valid[0] = 1; 

	    if (line -> dirty[0]) {
		line -> write_backs++;
	    }

	    line -> tags[0] = t;
	    line -> dirty[0] = make_dirty;

	} else {
	    //
	    // Find the oldest existing line, but also take the first empty line.
	    //
	    int oldest = -1;
	    int oldest_age = -1;
      
	    for (int a = 0; a < (1 << lg_assoc); a++ ) {
		if ( line -> valid[a] & line -> age[a] > oldest_age ) {
		    oldest = a;
		    oldest_age = line -> age[a];
		} else if ( ! line -> valid[a] ) {
		    line -> valid[a] = 1;
		    oldest = a;
		    break;
		}
	    }
	    //
	    // oldest is the one with the oldest age. We replace that one,
	    // but we update all ages first.
	    //
	    for (int aa = 0; aa < (1 << lg_assoc); aa++ ) {
		line -> age[aa] += 1;
	    }
	    //
	    // Make this line be the youngest
	    //
	    if (line -> valid[oldest] & line -> dirty[oldest]) {
		line -> write_backs++;
	    }

	    line -> tags[oldest] = t;
	    line -> dirty[oldest] = make_dirty;
	    line -> age[oldest] = 0;
	}
    }
  
    //
    // Return 1 if it's was a miss, 0 otherwise
    int load(address addr) {
	int l;
	address t;
	line_and_tag(l, t, addr);
	PerSet* line = &lines[l];

	line -> loads++;
	int miss;
	if ( !ref_if_hit( addr, line, l, t, 0 ) ) {
	    line -> load_misses ++;
	    miss = 1;
	    fill(addr, line, l, t, 0);
	} else {
	    miss = 0;
	}
	return miss;
    }

    //
    // This makes 'references' references to the SAME cache line
    // located at address 'addr'. This is mainly used for faster
    // instruction cache simulation.
    //
    int load_same_line(address addr, int references) {
	int l;
	address t;
	line_and_tag(l, t, addr);
	PerSet* line = &lines[l];


	line -> loads += references;
	int miss;
	if ( !ref_if_hit( addr, line, l, t, 0 ) ) {
	    //
	    // A miss counts for one miss, no matter how many loads there
	    // are to it.
	    //
	    line -> load_misses += 1;
	    miss = 1;
	    fill(addr, line, l, t, 0);
	} else {
	    miss = 0;
	}
	return miss;
    }

    int store_allocate(address addr) {
	int l;
	address t;
	line_and_tag(l, t, addr);
	PerSet* line = &lines[l];

	line -> stores++;
	int miss = 0;
	if ( !ref_if_hit( addr, line, l, t, 1 ) ) {
	    line -> store_misses++;
	    miss = 1;
	    fill(addr, line, l, t, 1);
	} 
	return miss;
    }

    int store_no_allocate(address addr) {
	int l;
	address t;
	line_and_tag(l, t, addr);
	PerSet* line = &lines[l];

	line -> stores++;
	int miss = 0;
	if ( !ref_if_hit( addr, line, l, t, 1) ) {
	    miss = 1;
	    line -> store_misses++;
	}
	return miss;
    }

    int flush_dirty() {
	int write_backs = 0;
	for (int l = 0 ; l < numlines; l++ ) {
	    for (int a = 0 ; a < (1 << lg_assoc); a++ ) {
		if ( lines[l].dirty[a] ) {
		    lines[l].write_backs++;
		    write_backs++;
		    lines[l].dirty[a] = 0;
		}
	    }
	}
	return write_backs;
    }

    //
    // Only changed if you use for_each_reference
    //
    long stores() {
	long foo = 0;
	for (long i=0; i < numlines; i++) {
	    foo += lines[i].stores;
	}
	return foo;
    }

    long loads() {
	long foo = 0;
	for (long i=0; i < numlines; i++) {
	    foo += lines[i].loads;
	}
	return foo;
    }
    long store_misses() {
	long foo = 0;
	for (long i=0; i < numlines; i++) {
	    foo += lines[i].store_misses;
	}
	return foo;
    }
    long load_misses() {
	long foo = 0;
	for (long i=0; i < numlines; i++) {
	    foo += lines[i].load_misses;
	}
	return foo;
    }

    long write_backs() {
	long foo = 0;
	for (long i=0; i < numlines; i++) {
	    foo += lines[i].write_backs;
	}
	return foo;
    }
    //
    // Statistics reporting
    //


    void fini(ostream& output) {
	output << "-----------------------------------";
	output << "-----------------------------------\n";
	output << "  ";
	output << (1 << lg_assoc) << "-way Assoc Cache, ";
	output << (1 << block_size_shift) << " byte lines, ";
	output << (1 << lglines) << " lines, ";
	int size =(1 << (block_size_shift + lglines));
	size /= 1024;
	output << size << "K bytes\n";
	output << "-----------------------------------";
	output << "-----------------------------------\n";


	int totals = loads() + stores();
	int total_misses = load_misses() + store_misses();

	output << "Load references  = " << loads() << "\n";
	output << "Load misses      = " << load_misses() << "\n";
	double load_miss_rate = load_misses();
	if ( loads() > 0 ) {
	    load_miss_rate /= loads();
	} else {
	    load_miss_rate = 0.0;
	}
	load_miss_rate *= 100.0;
	output << "Load miss rate   = " << load_miss_rate << "%\n";
	output << "\n";

	output << "Store references = " << stores() << "\n";
	output << "Store misses     = " << store_misses() << "\n";
	double store_miss_rate = store_misses();
	if ( stores() > 0 ) {
	    store_miss_rate /= stores();
	} else {
	    store_miss_rate = 0.0;
	}
	store_miss_rate *= 100.0;
	output << "Store miss rate  = " << store_miss_rate << "%\n";
	output << "\n";

	output << "Total references = " << totals << "\n";
	output << "Total misses     = " << total_misses << "\n";
	double total_miss_rate = total_misses;
	if ( totals > 0 ) {
	    total_miss_rate /= totals;
	} else {
	    total_miss_rate = 0.0;
	}
	total_miss_rate *= 100.0;
	output << "Total miss rate  = " << total_miss_rate << "%\n";
	output << "\n";

	output << "Write backs      = " << write_backs() << "\n";
    }

    void put_header(char *prefix, ostream& output) {
	output << prefix << '_';
    }


    void digestable_report(ostream& output, char *prefix) {
	int block_size = (1 << block_size_shift);
	int assoc = (1 << lg_assoc);
	int size = block_size * numlines * assoc;
	put_header(prefix, output);
	output << "block_size -> " << block_size << "\n";
	put_header(prefix, output);
	output << "lines -> " << numlines << "\n";
	put_header(prefix, output);
	output << "blocks -> " << numlines * assoc << "\n";
	put_header(prefix, output);
	output << "assoc -> " << assoc << "\n";
	put_header(prefix, output);
	output << "size -> " << size << "\n";

	unsigned long totals = loads() + stores();
	unsigned long total_misses = load_misses() + store_misses();

	put_header(prefix, output);
	output << "load_references  -> " << loads() << "\n";

	put_header(prefix, output);
	output << "load_misses -> " << load_misses() << "\n";
	double load_miss_rate = load_misses();
	if ( loads() > 0 ) {
	    load_miss_rate /= loads();
	} else {
	    load_miss_rate = 0.0;
	}
	load_miss_rate *= 100.0;

	put_header(prefix, output);
	output << "load_miss_rate -> " << load_miss_rate << "\n";

	put_header(prefix, output);
	output << "store_references -> " << stores() << "\n";
	put_header(prefix, output);
	output << "store_misses     -> " << store_misses() << "\n";
	double store_miss_rate = store_misses();
	if ( stores() > 0 ) {
	    store_miss_rate /= stores();
	} else {
	    store_miss_rate = 0.0;
	}
	store_miss_rate *= 100.0;
	put_header(prefix, output);
	output << "store_miss_rate  -> " << store_miss_rate << "\n";

	put_header(prefix, output);
	output << "total_references -> " << totals << "\n";
	put_header(prefix, output);
	output << "total_misses     -> " << total_misses << "\n";
	double total_miss_rate = total_misses;
	if ( totals > 0 ) {
	    total_miss_rate /= totals;
	} else {
	    total_miss_rate = 0.0;
	}
	total_miss_rate *= 100.0;
	put_header(prefix, output);
	output << "total_miss_rate  -> " << total_miss_rate << "\n";

	put_header(prefix, output);
	output << "write_backs      -> " << write_backs() << "\n";
    }

};

#endif
