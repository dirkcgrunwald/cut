//-*-c++-*-
#ifndef _rpcc_h_
#define _rpcc_h_


#ifndef INLINE
#  define INLINE __inline
#endif

#ifdef __cplusplus
#undef  INLINE
#define INLINE inline
extern "C" {
#endif

#ifndef __digital_unix
INLINE unsigned long raw_rpcc()
{
  static long x = 0;
  return x++;
}

#else
/*
 * Read the actual rpcc and 'fix' the value
 */

#ifndef __GNUC__
#include <c_asm.h>
#endif

INLINE unsigned long raw_rpcc()
{
#ifdef __GNUC__
  unsigned long x;
  asm("rpcc %0" : "&r=" (x));
  return x;
#else
  return asm("rpcc %v0");
#endif
}
#endif

INLINE
unsigned long
fix_rpcc(unsigned long long rpcc)
{
    unsigned long offset = (rpcc >> 32) & 0xffffffff;
    unsigned long cycles = (rpcc & 0xffffffff);
    return offset + cycles;
}

INLINE
unsigned long fixed_rpcc()
{
  return fix_rpcc(raw_rpcc());
}

/*
 * Return the time difference, in microseconds between
 * two rpcc's. This is scaled for the processor cycle time.
 */
double rpcc_delta(unsigned long ul_start, unsigned long ul_stop);

#ifdef __cplusplus
}
#endif

#endif
