//-*-C++-*-
#ifndef CUT_GRAPH_H
#define CUT_GRAPH_H

// Graph.h  - part of IDOM package
//
// This supplies the classes DomNode, GraphNode, GraphEdge, Graph.
// If the user wants to augment the node and edge types, he is expected
//	to pass in derived types. Hence, most interfaces push the "new"
//	operations off on the user side of the interface.
//	(add_single_{root,exit}_node, add_new_node, add_nodes are exceptions.)
// Graph methods include:
//	Constructor of empty graph.
//	add_node, add_nodes, add_new_node
//	add_edge
//	make_root,make_root_key	(the first add_node creates the default root)
//	make_exit,make_exit_key	(no default)
//	compute_dominators
//	immed_dominates		(test relationship of two nodes)
//	dominates		(test relationship of two nodes)
//	immed_post_dominates	(test relationship of two nodes)
//	post_dominates		(test relationship of two nodes)
//	back_edge
//	loop_header
//	add_single_exit_node
//	add_single_root_node
//
// Public fields of a GraphNode include:
//	Its key, indegree, and outdegree.
//	Sets of predecessors and successors.
//	GraphNode* to its parent in the dominator tree.
//	loop depth, and whether it is a loop header, and {loop body}
// Public fields of the Graph include:
//	the node count.
//	the maps (the RSTree) of 
//		key=> GraphNode
//		key=> DomNode in the dominator tree
//		key=> DomNode in the post-dominator tree
//	if the whole graph was reachable, and the maximum outdegree.
// Implementation:
//	As nodes are created, I keep them in an RSTree, ie a keyed list.
//	I create two matching DomNodes for each GraphNode, and they are
//	also kept in RSTrees.
//	As edges are created, I keep them in an SLList.
//	When compute_dominators is called, I traverse the edge list,
//	and fill out the successor and predecessor sets, and the indegree
//	and outdegree counts. 
//	From there the graph algorithm is from 
//		Lengauer and Tarjan, TOPLAS July 1979(1,1)p.121-141.
//	(They give a simple version and a sophisticated version: see ifdefs.)
//	However, we have to run L&T twice, once backward, since thats the
//	only algorithm I know of for getting the postdominator info.
//	The dominator and postdominator trees are built after the respective
//	runs of L&T.


// HISTORY:
// 6+oct94 Don Lindsay <lindsay@cs.colorado.edu>
//	Compiles/runs on Linux/g++ and Alpha OSF-1/cxx.
//	Runs correctly on example on Zima P.60 and on L&Ts Figure 1, and
//		on the Dragon book (Aho,Sethi,Ullman) p.603, and others.
//	This version of LT code uses arrays, not the GraphNodes.


// TODO:
// - the L&T "sophisticated" algorithm seg faults on the Alpha, but runs
//	fine on g++/Linux. For the moment its ifdefd out in Graph.C,
//	but we could debug it if we need speed. Probably something to do
//	with L&T using array indices where 0 was legal, whereas I used
//	pointers. Or maybe not, since it ran on Linux.
// - Possibly node splitting should do INDEGREE-way splits, or edge-aware
//	2-way splits, instead of the FIFO 2-way now used.
//	The L&T example needed 3 passes, and perhaps wider/smarter
//	splitting would speed that up.
// - Should adjust the code so that the postdominator doesnt have to
//	be computed inside the split loop. (See comments in code.)
// - Could add a .control_dependence method. Given a node N, returns
//   	the set that N dominates, but that dont post-dominate N.
// - It was suggested that a node could have a pointer to the immediately
//	surrounding loops header. Some sort of dominator tree walk
//	could probably generate these in one pass.

#include <Pix.h>
#include <assert.h>
#include <DLList.h>
#include <RSTree.h>
#include <iostream.h>
#include "bool.h"

class Graph;			// predeclaration
class GraphNode;

typedef unsigned long NodeKey;	// index type of RSTrees

//----------------------------------------------------------------------
//The user is expected to extend this via deriving personal classes,
//and passing those in where this is demanded.

class GraphEdge{
    	friend Graph;
   private:
   public:
	GraphNode *pfrom, *pto;		// public for testing reasons only

	GraphEdge(){ }
};

//----------------------------------------------------------------------
// The user is expected to extend this via deriving personal classes,
// and passing those in where this is demanded.

class GraphNode {
    friend Graph;

public:
    NodeKey key;	// The graph.add_node key value.
    // Finds this node in Graph.node_map, and in
    // 	Graph.dom_map and Graph.postdom_map.
    // If node splitting occurs, a unique key is
    //	synthesized.
    NodeKey split_from;
    // If node splitting occurs, this says who this
    //	node is a split of.

				// Except for key, there are no valid values
				// here until .compute_dominators has run.
    int indegree, outdegree;
    DLList<GraphEdge*> predecessors;	// note, ->edge not node
    DLList<GraphEdge*> successors;	

    DLList<GraphNode*> nat_loop;	
    int visited;
    int in_loop;
				// Above values final after "step 0".

    GraphNode *dom;			// -> immediate dominator
    GraphNode *post_dom;		// -> immediate post_dominator
				// (see reinit_nodes())
    bool is_loop_header;
    int  loop_depth;
    RSTree<NodeKey,GraphNode*> loop_body_map;
    int ltindex;			//index in ltarray

    GraphNode() 
    { 	
	split_from = 0;
	is_loop_header = (bool)FALSE;
	loop_depth = 0;
	dom = post_dom = 0; 	// see reinit_nodes()
	indegree = outdegree = 0;
	rpostorder = 0;
	lpostorder = 0;
	visited = FALSE;
	in_loop = FALSE;
    }

    int rpostorder;		// Zima algorithm 3.2 p.66
    int lpostorder;
};

//----------------------------------------------------------------------
class Graph
{
public:
    class DomNode {
	friend Graph;
    private:
	int left_num;	// Counter value during a postorder treewalk.
	int right_num;	// Counter value during a preorder treewalk.
	// For two nodes A and B in the same tree,
	// (A.left_num < B.left_num and
	//  A.right_num > B.right_num) iff A is a member
	// of Bs subtree.
    public:
	NodeKey key;	// The graph.add_node key value.
	// Finds this node in Graph.node_map, and in
	// 	Graph.dom_map and Graph.postdom_map.
	DomNode *parent;
	int outdegree;			// == length of children list
	DLList<DomNode*> children;

	DomNode(){ outdegree = 0; parent = 0; }
	// Note that unreachable nodes will remain zero
    };



    class LT{
	friend Graph;
    public:
	// All varnames in this struct are as Lengauer&Tarjan used.
	// None of the vars are interesting except while 
	//	method .compute_dominators is running.

	GraphNode *node;
	int parent;	// in the step-1 spanning tree.
				// Above values final after "step 1".

	int semi;		//  Initted at node creation
				//  and at reinit_nodes()
	int dom;
	int label;	
	int ancestor;	
	int child;	
	int size;		
	// Above values set in step 1 & then changed.

	DLList<int> bucket;
	// Above values set in steps 2&3.
	LT(){
	    semi = 0;		// see reinit_nodes()
	}
    };

private:
    int *vertex;	// L&T name: array of node#->node. Unit origin
    LT *ltarray;
    int node_upcounter;	// L&T called this n
    int node_downcounter;	// Zima computation of rpostorder

    // The above built during step 1.

    // Internal routines, see Graph.C for bodies & docn:
    void propagate_edges_to_nodes();
    void depth_first_search( bool, int, DLList<GraphEdge*>* );
    void reverse_depth_first_search( GraphNode * node);
    void number_the_nodes( bool );
    void reverse_number_the_nodes();
    void LT_link( int, int );
    void compress( int );
    int LT_eval( int );
    void step23( bool );
    void step4( bool );
    void order_dom_tree( DomNode * );
    void build_dom_tree( bool );
    void compute_lengauer( bool );
    void reinit_nodes();
    void find_loop_body( GraphNode *, GraphNode * );
    void find_loops();
    void really_add_single_exit_node();
    void really_add_single_root_node( NodeKey );
    void split_this( GraphNode * );
    bool split_nodes();
    void reinit_tree( bool );

    bool make_single_exit_node;
    bool make_single_root_node;
    NodeKey pending_base_root;		// to be used by really_..
    NodeKey max_key;

public:
    DLList<GraphEdge*> edge_list;

    // Each node is represented 3 times: 1 in a graph, 2 in trees.
    // They share the key value handed in via the .add_node method.
    RSTree<NodeKey,GraphNode*> node_map;
    RSTree<NodeKey,DomNode*>   dom_map;
    RSTree<NodeKey,DomNode*>   postdom_map;

    GraphNode *graph_root;	 // the distinguished node (or null)
    GraphNode *graph_exit;	 // the other distinguished node (or null)
				// (dont do postdoms if no exit)

    DomNode *dom_root;	 // see reinit_nodes()
    DomNode *postdom_root;	 // see reinit_nodes()

    int node_count;		 // # of times .add_node was called
    int node_reachable_count;// set <= node_count by .compute_dominators
    bool all_reachable;	 // set by .compute_dominators
    int max_outdegree;	 // set by .compute_dominators
    int exit_indegree;	 // set by .compute_dominators
    int root_outdegree;	 // set by .compute_dominators

    //---------------------------------------------------------
    // Constructor

    Graph();
    void graph_delete();

    void show_nodes(ostream & out);
    void show_loop_headers(ostream & out);
    void show_loop_body(ostream & out, GraphNode *node);
    void show_loop_bodies(ostream & out);
    void show_back_edge(ostream & out);
    void show_loop_exits(ostream & out);
    void show_dominance(ostream & out);
    void show_post_dominance(ostream & out);
    void show_all(ostream & out, char * name);

    //---------------------------------------------------------
    // Expects user did a "new GraphNode" or equiv on a derived class.
    // Adds the node to the graph.
    // This node becomes the graphs default root, 
    //	if this is the first node to be added to this graph.
    // Call this before .compute_dominators, and not afterwards.

    void add_node( GraphNode *pn, NodeKey key );

    // variant of above that news the GraphNode for you,
    // and is a no-op if a node of that key exists already.

    void add_new_node( NodeKey key );

    //---------------------------------------------------------
    // Looks the keys up in the node maps. Creates nodes if they dont
    // 	already exist: i.e. may create 0,1 or 2 nodes.
    // Creates an edge from node1 to node2. Adds all this to the graph.
    // The key1 node becomes the graphs default root, 
    //	if this is the first node to be added to this graph.
    // Note that created edges and nodes are the base class, and you
    //	probably shouldnt mix derived and base nodes. So, if
    //	are augmenting GraphNode or GraphEdge, this routine may
    //	be inappropriate, and .add_node is a better choice.
    // Call this before .compute_dominators, and not afterwards.

    void add_nodes( NodeKey key1, NodeKey key2 );

    //---------------------------------------------------------
    // Expects user did a "new GraphEdge(from,to)", or equiv on a
    //  derived class.
    // Adds the edge to the graph.
    // Call this before .compute_dominators, and not afterwards.

    inline void add_edge( GraphEdge *new_edge, 
			  GraphNode *from, GraphNode *to )
    {
	new_edge->pfrom = from;
	new_edge->pto   = to;
	edge_list.append( new_edge );
    }
    //---------------------------------------------------------
    // A node already in the graph becomes the root 
    //	(==distinguished node of the graph ).
    // If you do not call this, the first call to .add_node is taken
    //	to have added the root to the graph.
    // Alternatively, you can call add_single_root_node.

    inline void make_root( GraphNode *pn )
    {
	graph_root = pn;
    }
    void make_root_key( NodeKey key );	// slight variant of above
    //---------------------------------------------------------
    // A node already in the graph becomes the exit
    //	(==root of the graph when computing postdomination).
    // If you do not call one of these (or call add_single_exit_node()) 
    //	postdomination wont be computed, since it has to know a exit.

    void make_exit( GraphNode *pn );	// Two different ways to
    void make_exit_key( NodeKey key );	// .. specify the node
    //---------------------------------------------------------
    // This method is the Lengauer/Tarjan algorithm.
    // You must have done .add_node and .add_edge calls first (and
    // 	not after this). This method build/computes everything,
    //	and query methods will work after this returns.

    void compute_dominators();
    //---------------------------------------------------------
    // The keys are used to look up nodes in our maps.
    // Reports if the first node is the immediate dominator of the 2nd.
    // User must first call compute_dominators once on a nonempty graph.

    bool immed_dominates( NodeKey key_a, NodeKey key_b );

    //---------------------------------------------------------
    // The keys are used to look up nodes in our maps.
    // Reports if the first node is a dominator of the second.
    // User must first call compute_dominators once on a nonempty graph.

    bool dominates( NodeKey key_a, NodeKey key_b );

    //---------------------------------------------------------
    // The keys are used to look up nodes in our maps.
    // Reports if the first node is the immediate postdominator of the 2nd.
    // User must first call compute_dominators once on a nonempty graph.

    bool immed_post_dominates( NodeKey key_a, NodeKey key_b );

    //---------------------------------------------------------
    // The keys are used to look up nodes in our maps.
    // Reports if the first node is a postdominator of the second.
    // User must first call compute_dominators once on a nonempty graph.

    bool post_dominates( NodeKey key_a, NodeKey key_b );

    //---------------------------------------------------------
    // The keys are used to look up nodes in our maps.
    // Assumes there is an edge from node "from" to node "to".
    // Reports true if such an edge would be a back edge.
    // User must first call compute_dominators once on a nonempty graph.

    bool back_edge( NodeKey key_from, NodeKey key_to );

    //---------------------------------------------------------
    // The key is used to look up a node in our maps.
    // Reports true if the node is a loop header.
    // ( Not defined as: there is an edge to it from a node it dominates.
    //   Instead defined as: there is an edge to it that is a back edge.)
    // User must first call compute_dominators once on a nonempty graph.

    bool loop_header( NodeKey key );

    //---------------------------------------------------------
    // This method leaves a note for compute_dominators to synthesize
    // 	an exit node, and make the graph single-exit.
    // A unique key will be generated.
    // All nodes with outdegree zero will be given edges to this exit node.
    // The indegree of this new node will be available (after
    //	compute_dominators is finished) in the .exit_indegree variable.

    void add_single_exit_node();

    //---------------------------------------------------------
    // This method leaves a note for compute_dominators to synthesize
    // 	a root node, and make the graph single-entry.
    // A unique key will be generated.
    // All nodes with indegree zero will be given edges from this root.
    // Also, an edge will be added from this root to the base_root,
    //	if it exists.
    // The outdegree of this new node will be available (after
    //	compute_dominators is finished) in the .root_outdegree var.

    void add_single_root_node( NodeKey base_root);

    //---------------------------------------------------------
    // This method expects that the two keys represent nodes with
    //	an edge between them.
    // If "from" has a loop depth of zero, returns FALSE.
    // 	Else, the closest loop containing "from" is identified.
    //	Returns FALSE if that loop also contains "to", else TRUE.
    // User must first call compute_dominators once on a nonempty graph.

    int loop_exit( NodeKey from, NodeKey to );

    void compute_nat_loops();

    int make_nat_loop(GraphNode * loop_head, GraphNode * node);

    void add_to_nat_loop(GraphNode * loop_head, GraphNode * node);
};

#endif
