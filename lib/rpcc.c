#ifndef __digital_unix
double
rpcc_delta(unsigned long ul_start, unsigned long ul_stop)
{
  return 1.0;
}
#else

#include <sys/sysinfo.h>
#include <machine/hal_sysinfo.h>
#include <machine/rpb.h>

static double scale = -1;

double
rpcc_delta(unsigned long ul_start, unsigned long ul_stop)
{

    if ( scale == -1 ) {
	  int         err;
	  double freq;
	  struct rpb  rpbbuf;

	  err = getsysinfo(GSI_GET_HWRPB, &rpbbuf, sizeof(rpbbuf));
	  if (err < 0) {
	      freq = 233;
	  } else {
	      freq = rpbbuf.rpb_counter;
	  }
	  /*
	   * Scale is the 1 over the clock frequency, in microseconds
	   */
	  scale = (1000.0 * 1000.0)/(double) freq;
    }

    if (ul_stop < ul_start ) {
	unsigned long delta;
	delta = 0xffffffff - ul_start;
	delta += ul_stop;
	ul_start = 0;
	ul_stop = delta;
    }

    return (ul_stop - ul_start) * scale;
}

#endif
