//-*-c++-*-
#ifndef _CUT_TLBFlex_h_
#define _CUT_TLBFlex_h_

//
// Simulate an entries-entry TLB using LRU replacement.
//
// This class is essentially like TLBV, but uses the "list" (i.e.,
// run-time configurable) interface rather than the "vector" (i.e., compile-time
// configurable interface).
//

#include "functional"
#include "CUT_MTFL.h"


template <int lg_page_size>
class CUT_TLBFlex {
  
  typedef equal_to<unsigned long> MTFL_uleqtype;

  //
  // The following describes a single "bank" of the TLB.
  // For example, a 64-entry, 4-way associative TLB would
  // be configured as 16 "banks" of four entries each.
  //
  class bank {
  public:
    bank() : MTFL() {
      refs = 0;
      misses = 0;
      evictions = 0;
    }

    CUT_MTFL<unsigned long, MTFL_uleqtype> MTFL;
    unsigned long refs;
    unsigned long misses;
    unsigned long evictions;
  };
  
  //
  // Each bank contains ENTRIES/BANKS entries
  //
  bank *tlb;
  int lg_num_banks;
  int entries_per_bank_;
public:
 
  struct decoded {
    int bank_index;
    unsigned long page_offset;
  };

public:
  CUT_TLBFlex(short _entries_per_bank, int lg_number_of_banks) 
  {
    lg_num_banks = lg_number_of_banks;
    entries_per_bank_ = _entries_per_bank;

    tlb = new bank[1 << lg_num_banks];

    for (int i = 0; i < (1 << lg_num_banks); i++) {
      tlb[i].MTFL.resize(entries_per_bank, 0);
    }
  }

  ~CUT_TLBFlex() {
    //
    // Nothing to do - all static storage
    //
  }

  void map_addr(unsigned long addr, decoded& x) {
    unsigned long page = addr >> lg_page_size;

    x.bank_index = ((1 << lg_num_banks) - 1) & page;
    x.page_offset = page;
  }

  //
  // Takes a page reference
  //
  //
  // returns TRUE if this is a miss, FALSE if it's a hit
  //

  bool check_for_hit(decoded&x ) {
    bank& b = tlb[x.bank_index];
    b.refs++;

    if ( b.MTFL.find_and_move_to_front(x.page_offset) ) {
      //
      // It's a hit - no need for replacement..
      //
      return false;
    }
    b.misses++;
    return true;
  }

  //
  // Update the TLB to include the reference for this page.
  // We assume that the page is not already in the TLB, but
  // we do not check for that condition. You should only
  // enter a page if you got a TRUE from the check_for_hit routine.
  //
  void update_tlb(decoded& x) {
    bank& b = tlb[x.bank_index];
    b.MTFL.lru_replacement(x.page_offset);
    b.evictions++;
  }

  void digestable_report(ostream& out, char* prefix) {

    unsigned long total_refs = 0;
    unsigned long total_misses = 0;
    unsigned long total_evictions = 0;

    const int max_banks = (1 << lg_num_banks);

    out << prefix << "_banks" << max_banks << endl;
    out << prefix << "_entries_per_bank" << entries_per_bank << endl;
    out << prefix << "_total_entries" << entries_per_bank * max_banks  << endl;

    for (int i=0; i < max_banks; i++) {
      unsigned long refs = tlb[i].refs;
      unsigned long misses = tlb[i].misses;
      unsigned long evictions = tlb[i].evictions;

      total_refs += refs;
      total_misses += misses;
      total_evictions += evictions;

      out << prefix << "_bank" << i << "_refs -> " << tlb[i].refs << endl;
      out << prefix << "_bank" << i << "_misses -> " << tlb[i].misses << endl;
      out << prefix << "_bank" << i << "_evictions -> " << tlb[i].evictions << endl;
    }

    out << prefix << "_total_refs -> " << total_refs << endl;
    out << prefix << "_total_misses -> " << total_misses << endl;
    out << prefix << "_total_evictions -> " << total_evictions << endl;
  }
};
#endif

