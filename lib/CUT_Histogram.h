//-*-c++-*-
#ifndef _CUT_Histogram_h_
#define _CUT_Histogram_h_

#include "CUT_Statistic.h"
#include <iostream>
#include <assert.h>
#include <map>
#include <iomanip>

using namespace std;

template <class T = double>
class CUT_Histogram : public CUT_Statistic<T> {

  typedef map<T, long> HistMap;
  typedef typename HistMap::iterator HistMapIt;

  HistMap map;
  long num_samples;
  int num_quantiles;

  
public:
  CUT_Histogram() {
    num_samples = 0;
    num_quantiles = 10;
  }

  CUT_Histogram(int quantiles) {
    num_samples = 0;
    num_quantiles = quantiles;
  }

  virtual ~CUT_Histogram() {
    // empty
  }

  double median() {
    return quantile(0.50);
  }

  T quantile(double cut) {
    double sum = 0;
    double Quantile50 = (double) num_samples * cut;

    HistMapIt item;
    for (item = map.begin(); item != map.end(); item++) {
      double value = (*item).first;
      double times = (*item).second;
      sum += times;

      if (Quantile50 <= sum) {
	return value;
      }
    }

    return (-1);
  }

  virtual void reset() {
    CUT_Statistic<T>::reset();
    map.erase(map.begin(), map.end());
    num_samples = 0;
  }

  virtual void add(T value, long samples = 1) {
    CUT_Statistic<T>::add( value, samples );

    num_samples += samples;
    int key = int(value);

    HistMapIt d = map.find( key );
    if ( d == map.end() ) {
      map[key] = samples;
    } else {
      long& s = (*d).second;
      s += samples;
    }
  }

  virtual void out(ostream& out) {
    double quantiles[10];

    if (num_quantiles > 10) {
      num_quantiles = 10;
    }

    double interval_inc = 1.0 / num_quantiles;

    int i;
    for (i=0; i < num_quantiles; i++) {
      quantiles[i] = ((double) num_samples * (i+1.0) * interval_inc);
    }
    double median = (num_samples / 2);

    CUT_Statistic<T>::out(out);
    out << "samples -> " << num_samples << "\n";
    int looking_for_median = 1;
    i = 0;

    double sum = 0.0;
    HistMapIt item;
    for (item = map.begin(); item != map.end(); item++)
      {
	double value = (*item).first;
	double times = (*item).second;
	sum += times;
	if ((i < num_quantiles) && (quantiles[i] <= sum)) {
	  i++;
	  out << "quantile-" << (((double) i) * interval_inc);
	  out << " -> " << setw(4) << value << "\n";
	}

	if (looking_for_median && (median <= sum)) {
	  looking_for_median = 0;
	  median = value;
	}
      }
    out << "median -> " << median << "\n";
    double cum = 0.0;
    for (item = map.begin(); item != map.end(); item++) {
      double value = (*item).first;
      double times = (*item).second;

      double percent = times;
      percent /= num_samples;
      percent *= 100;
      cum += percent;

      out << setw(8) << value << " -> " << setw(8) << times;
      out << " (" << setw(4) << percent << "%, " << setw(4) << cum << "%) ";
      out<< "\n";
    }
  }

  virtual void digestable_report(ostream& out, char* prefix) {
    double quantiles[10];

    if (num_quantiles > 10) {
      num_quantiles = 10;
    }

    double interval_inc = 1.0 / num_quantiles;

    int i;
    for (i=0; i < num_quantiles; i++) {
      quantiles[i] = ((double) num_samples * (i+1.0) * interval_inc);
    }

    CUT_Statistic<T>::digestable_report(out, prefix);
    out << prefix << "_quantile_samples -> " << num_samples << "\n";

    double sum = 0;

    HistMapIt item;
    for (item = map.begin(); item != map.end(); item++)
      {
	double value = (*item).first;
	double times = (*item).second;
	sum += times;
	if ((i < num_quantiles) && (quantiles[i] <= sum)) {
	  i++;
	  out << prefix << "_quantile_" << (((double) i) * interval_inc);
	  out << " -> " << value << "\n";
	}
      }

    double cum = 0.0;
    for (item = map.begin(); item != map.end(); item++) {

      double value = (*item).first;
      double times = (*item).second;

      double percent = times;
      percent /= num_samples;
      percent *= 100;
      cum += percent;

      out << prefix << "_Samples_" << value << " -> " << times << "\n";
      out << prefix << "_Percent_" << value << " -> " << percent << "\n";
      out << prefix << "_CumPercent_" << value << " -> " << cum << "\n";
    }

  }
};


#endif


