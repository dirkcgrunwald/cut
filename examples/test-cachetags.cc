//#define CACHE_TYPE_THRESHOLD 1024

#include "CUT_CacheDefs.h"
#include "CUT_Cache.h"
#include "CUT_Histogram.h"
#include "rpcc.h"

const int howmany = 1000000;
char *spec = "32768:32:1";

//
// Our customized tags
//

CUT_Histogram<double> stores_before_writeback;

class MyTag : public TagVIVT {
  int stores;
public:
  MyTag() {
    stores = 0;
  }

  MyTag(const CacheDefs& cd, CacheDefs::address vaddr_) : TagVIVT(cd, vaddr_)
  {
    stores = 0;
  }

  virtual void store(CacheDefs& cd, TagVIVT& addr, int access_size)
  {
    stores++;
  }

  virtual void writeback(CacheDefs& cd, TagVIVT& addr, int access_size)
  {
    stores_before_writeback.add(stores);
  }
};

#define CHOOSE_LOAD (random() & 0x1)
// #define CHOOSE_LOAD 1

void test_vivt()
{
  CacheDefs cd("low_vivt", spec);
  typedef Cache<MyTag> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();

  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0x7fffff;
    MyTag tag(cd, addr);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";

  cache.short_output(cout);

  stores_before_writeback.digestable_report(cout,"stores_");
}

int
main()
{
  srandom(0);
  test_vivt();
  return (0);
}
