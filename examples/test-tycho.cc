#include <iostream>
#include "CUT_TychoCache.h"
#include <stdlib.h>

int
main()
{
  //
  //
  //  long howmany = 10000000;
  long howmany = 1000000;
  cout << "Insert " << howmany << " TLB references into a small 2-way associative TLB\n";

  CUT_TychoCache cache(32, 7, 12, 8);

  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0x7fff;
    cache.for_each_reference(addr);
  }
    
  cache.fini(cout);
  //
  // Now, dump it out in a machine readable form...
  //
  cout << "Here's outputmetrics" << endl;
  cache.outputmetrics(cout);
  cout << "Here's outputmissratios" << endl;
  cache.outputmissratios(cout);
  cout << "Here's outputmissdata" << endl;
  cache.outputmissdata(cout, "data");
}

