//-*-c++-*-
#ifndef _CUT_OptCache_h_
#define _CUT_OptCache_h_

#include <iostream>
#include <ostream>
#include <stdlib.h>
#include <assert.h>

using namespace std;

class OT_HT;

class CUT_OptCache {
public:
  typedef unsigned long Addr;
  typedef unsigned long *AddrPtr;

  //
  // It is important to have LA_DIST be a Po2, otherwise we start
  // using division and things get very slow.
  //
#define LA_DIST (1<<17)
#define HASHNO 7211
  //    const int LA_DIST = 100000;
  //    const int HASHNO = 7211;

  protected:

    unsigned long addr_array[ 2 * LA_DIST ];
    unsigned long time_array[ 2 * LA_DIST ];
    int la_limit;
    int base;
    int oe_flag;
    int p_inum;

    OT_HT *ft_slot[HASHNO];

    // Line field width
    int L;

  protected:
  //
  // Initialize with L line field with (e.g., 32 bytes);
  //
  CUT_OptCache(int L_);

  virtual int stack_proc(int, int) = 0;
  virtual void inf_handler(Addr, int) = 0;

  //
  // Hash table manipulation routines.
  //
  int ft_hash(Addr);
    void ft_hash_del(Addr);

    int print_cachesize(ostream& out, int cachesize);

  public:
    void for_each_reference( CUT_OptCache::Addr );
    void fini(ostream&);
    virtual void output(ostream& ) = 0;
    void residual();
};

#endif
