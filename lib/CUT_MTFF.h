//-*-c++-*-
#ifndef _CUT_MTFF_h_
#define _CUT_MTFF_h_
#include <assert.h>

//
// A move-to-front list implemented using a dynamically allocated
// table. Lookup is accelerated by using hash table to locate the
// appropriate keys.
//

template<class T, class Compare, int hashsize = 64>
class CUT_MTFF
{
  class entry {
  public:
    //
    // next & prev pointers are stored as int indicies into the
    // storage of a bank. This save space on an Alpha, and means an
    // 'entry' takes 1/2 a cache line for e.g., a TLB simulation.
    //
    short next;
    short prev;

    short hash_next;
    short hash_prev;

    short valid;
    T contents;

    short get_chain() {
      return valid - 1;
    }

    void set_chain(short x) {
      valid = x+1;
    }

    void unlink(entry* entries) {
      entries[next].prev = prev;
      entries[prev].next = next;
    }

    void hash_unlink(entry* entries) {
      entries[hash_next].hash_prev = hash_prev;
      entries[hash_prev].hash_next = hash_next;
    }

    //
    // place 'me' before 'before' in the list, assuming
    // that I'm not already in the list
    //
    void link_before(entry* entries, short me, short before) {
      //
      // Fix myself
      //
      prev = entries[before].prev;
      next = before;

      //
      // now fix before (my next)
      //
      entries[before].prev = me;
      //
      // and my prev..
      //
      entries[prev].next = me;
    }

    void hash_link_before(entry* entries, short me, short before) {
      hash_prev = entries[before].hash_prev;
      hash_next = before;
      entries[before].hash_prev = me;
      entries[hash_prev].hash_next = me;
    }

    void print(int me, ostream& out) {
      out << "[ " << me << ": c = " << contents << ", next = " << next;
      out << ", prev = " << prev << ", hash_next = " << hash_next;
      out << ", hash_prev = " << hash_prev << ", valid = " << valid << "]" << endl;
    }
    
  };

  short lru_root;
  static const int hashtable_null = -1;
  short hashtable[hashsize];
  //
  // In practice, this has zero size..
  //
  Compare compare;
  short max_entries;
  entry *storage;

public:
  CUT_MTFF() {
    lru_root = 0;
    storage = 0;
    max_entries = 0;
  }

  void resize(short entries) {
    lru_root = 0;
    for (int i = 0; i < hashsize; i++) {
      hashtable[i] = hashtable_null;
    }
    max_entries = entries;
    storage = new entry[max_entries];

    storage[0].next = 0;
    storage[0].prev = 0;
    storage[0].hash_next = 0;
    storage[0].hash_prev = 0;
    storage[0].valid = 0;

    //
    // Thread the free list of the storage
    //
    for (short j = 1; j < max_entries; j++ ) {
      storage[j].link_before(storage, j, lru_root);
      storage[j].valid = 0;
    }
  }

  //
  // return TRUE if found in the list, FALSE otherwise
  //
  bool find(T& what) {
    int hash = gethash(what);
    short start = hashtable[hash];
    if (start = hashtable_null ) {
      return false;
    } else {
      short here = start;
      do {
	entry& h = storage[here];
	if ( h.valid & compare(h.contents, what) ) {
	  return true;
	}
	here = storage[here].next;
      } while (here != start);
      return false;
    }
  }

  //
  // return TRUE if found in the list, FALSE otherwise
  // As a side effect, move the referenced item to the front
  // of the list
  //

  bool find_and_move_to_front(T& what) {
    int hash = gethash(what);
    short start = hashtable[hash];

    if (start == hashtable_null ) {
      return false;
    } else {
      short here = start;
      do {
	entry& h = storage[here];

	//	cerr << "Examine "; h.print(here, cerr);

	//	assert(h.get_chain() == hash);

	if ( compare(h.contents, what) ) {
	  if ( here != lru_root ) {
	    //
	    // Update LRU order
	    //
	    storage[here].unlink(storage);
	    storage[here].link_before(storage, here, lru_root);
	    lru_root = here;
	  }
	  //	  cerr << "Found it, return" << endl;
	  hashtable[hash] = here;
	  return true;
	}
	here = storage[here].hash_next;
      } while (here != start);
      return false;
    }
  }

  T& front() {
    return storage[lru_root];
  }

  //
  // LRU replacement
  //
  void lru_replacement(T& what) {

    //
    // The item at lru_root is the least referenced
    // item, and will be allocated for the new item.
    //

    //
    // Check if LRU item is already on a hash list...
    //
    int lru = storage[lru_root].prev;
    entry& lr = storage[lru];

    if ( lr.valid ) {
      //      cerr << "LRU is valid, deallocate this item" << endl;
      //      lr.print(lru, cerr);
      int bucket = lr.get_chain();

      //
      // If this is the only thing in the hash chain
      // just clear the chain pointer
      //
      lr.hash_unlink(storage);
      if ( lr.hash_next == lru ) {
	hashtable[bucket] = hashtable_null;
      } else if ( hashtable[bucket] == lru ) {
	//
	// The bucket is pointing to this item..
	//
	hashtable[bucket] = lr.hash_next;
      }
    }

    int hash = gethash(what);
    storage[lru].contents = what;
    storage[lru].set_chain(hash);
    lru_root = lru;

    //
    // Now, place it on the proper chain
    //
    if ( hashtable[hash] == hashtable_null ) {
      //
      // Nothing on this chain..
      //
      lr.hash_next = lr.hash_prev = lru;
    } else {
      lr.hash_link_before(storage, lru, hashtable[hash]);
    }

    hashtable[hash] = lru;

    //    cerr << "Hash chain for " << hash << " now points to " << endl;
    //    lr.print(lru, cerr);
  }

  int gethash(T& what) {
    return what.tag() & (hashsize-1);
  }

  void print_chain(T& what, char *prefix, ostream& out) {
    int hash = hashclass(what);
    int start = hashtable[hash];
    if ( start == hashtable_null ) {
      out << prefix << "Chain empty" << endl;
    } else {
      int here = start;
      do {
	entry& h = storage[here];
	out << prefix; h.print(here, out);
	here = h.hash_next;
      } while( here != start );
    }
  }

};

#endif
