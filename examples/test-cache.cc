//#define CACHE_TYPE_THRESHOLD 1024

#include "CUT_CacheDefs.h"
#include "CUT_Cache.h"
#include "rpcc.h"
#include <stdlib.h>

const int howmany = 1000000;
char *spec = "32768:32:1";

#define CHOOSE_LOAD (random() & 0x1)
// #define CHOOSE_LOAD 1

void test_low_vivt()
{
  CacheDefs cd("low_vivt", spec);
  typedef Cache<TagVIVT, CacheLowAssoc> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();

  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0x7fff;
    TagVIVT tag(cd, addr);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";

  cache.short_output(cout);
}


void
test_low_vipt()
{

  CacheDefs cd("low_vipt", spec);

  typedef Cache<TagVIPT, CacheLowAssoc> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();
  for (int i = 0; i < howmany ; i++) {
    unsigned long vaddr = ::random() % 0x7fff;
    unsigned long paddr = vaddr;

    TagVIPT tag(cd, vaddr, paddr);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";
    
  cache.short_output(cout);
}


void
test_low_viptpid()
{

  CacheDefs cd("low_viptpid", spec);

  typedef Cache<TagVIPTpid, CacheLowAssoc> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();
  for (int i = 0; i < howmany ; i++) {
    unsigned long vaddr = ::random() % 0x7fff;
    unsigned long paddr = vaddr;
    int pid = 0;

    TagVIPTpid tag(cd, vaddr, paddr, pid);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";
    
  cache.short_output(cout);
}

void
test_high_vivt()
{

  CacheDefs cd("high_vivt", spec);
  typedef Cache<TagVIVT> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();
  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0x7fff;
    TagVIVT tag(cd, addr);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }

  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";
    
  cache.short_output(cout);
}


void
test_high_vipt()
{

  CacheDefs cd("high_vipt", spec);

  typedef Cache<TagVIPT> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();
  for (int i = 0; i < howmany ; i++) {
    unsigned long vaddr = ::random() % 0x7fff;
<    unsigned long paddr = vaddr;

    TagVIPT tag(cd, vaddr, paddr);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";
    
  cache.short_output(cout);
}

void
test_high_viptpid()
{

  CacheDefs cd("high_viptpid", spec);

  typedef Cache<TagVIPTpid> MyCache;
  MyCache cache(cd);

  unsigned long start = fixed_rpcc();
  for (int i = 0; i < howmany ; i++) {
    unsigned long vaddr = ::random() % 0x7fff;
    unsigned long paddr = vaddr;
    int pid = 0;

    TagVIPTpid tag(cd, vaddr, paddr, pid);

    if ( CHOOSE_LOAD) {
      cache.load(tag);
    } else {
      cache.store_allocate(tag);
    }
  }
  unsigned long stop = fixed_rpcc();
  cout << "Took " << rpcc_delta(start, stop) << " micro seconds\n";
    
  cache.short_output(cout);
}

int
main()
{
  srandom(0);
  test_low_vivt();
  cout << "-----------------------------------------------------------------------------\n";
  srandom(0);
  test_low_vipt();
  cout << "-----------------------------------------------------------------------------\n";
  srandom(0);
  test_low_viptpid();
  cout << "-----------------------------------------------------------------------------\n";
  srandom(0);
  test_high_vivt();
  cout << "-----------------------------------------------------------------------------\n";
  srandom(0);
  test_high_vipt();
  cout << "-----------------------------------------------------------------------------\n";
  srandom(0);
  test_high_viptpid();
  cout << "-----------------------------------------------------------------------------\n";
  exit(0);
}
