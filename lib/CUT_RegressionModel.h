//-*-c++-*-
#ifndef _CUT_RegressionModel_h_
#define _CUT_RegressionModel_h_
/**
 * A SimpleRegressionModel fits a least-squares data set to a set of
 * experimental (x,y) data obtained from a SimpleRegressionData.  You
 * must provide a SimpleRegressionData set and a significance
 * level for all the confidence estimates. E.g., 0.95 is a 95% CI
 *
 */

#include <math.h>
#include "CUT_StudentTDistribution.h"

template <class T>
class CUT_RegressionModel {

public:
  CUT_RegressionModel(CUT_RegressionData<T> data, double conf_level) {
    reset(data, conf_level);
  }

  void reset (CUT_RegressionData<T> data, double conf_level) {

    n = data.x.samples();
    alpha = conf_level;
    dof = n - 2;

    tdof = CUT_StudentTDistribution::tval((1.0 + conf_level) * 0.5, dof);

    sxy = data.sum_x_y;

    mx = data.x.mean();
    sx = data.x.sum();
    ssx = data.x.sum_squared();

    my = data.y.mean();
    sy = data.y.sum();
    ssy = data.y.sum_squared();

    b1 = (sxy - n * mx * my) / (ssx - n * mx * mx);
    b0 = my - b1 * mx;

    ss0 = n * my * my;
    sst = ssy - ss0;
    sse = ssy - b0 * sy - b1 * sxy;
    ssr = sst - sse;
    rsquared = ssr / sst;
    r = sqrt(rsquared);

    mse = sse / (n-2);
    se = sqrt(mse);

    stddevb0 = se * sqrt( 1/n + ((mx * mx) / (ssx - n * mx * mx)) );
    stddevb1 = se / sqrt( ssx - n * mx * mx);

    cib0 = tdof * stddevb0;
    cib1 = tdof * stddevb1;

    //
    // The following terms are needed for computing
    // the StdDevYmp for predictions
    //
    ssx_minus_nmxsq = ssx - n * mx * mx;
    ninv = 1 / n;

  }

  /**
   * Return B0 parameter
   */
  double B0() {
    return b0;
  }

  /**
   * Return B1 parameter
   */
  double B1() {
    return b1;
  }

  /**
   * Sum of squares Y, Jain, top of 227
   */
  double SSY() {
    return ssy;
  }

  /**
   * Sum of squares Y-mean, Jain, top of 227
   */
  double SS0() {
    return ss0;
  }

  /**
   * Sum of squares total, Jain, top of 227.
   */

  double SST() {
    return sst;
  }

  /**
   * Sum of squares error, Jain, bottom of page 227
   */
  double SSE() {
    return sse;
  }

  /**
   * Sum of squares regression, Jain, page 227
   */
  double SSR() {
    return ssr;
  }

  /**
   * Coefficient of determination
   */
  double RSquared() {
    return rsquared;
  }

  /**
   * Sample correlation coefficient
   */
  double R() {
    return r;
  }

  /**
   * Mean squared error
   */
  double MSE() {
    return mse;
  }

  /**
   * Standard variance of the errors
   */
  double SeSquared () {
    return mse;
  }

  double StdDevErr() {
    return se;
  }

  /**
   * Standard deviation of the B0 parameter. 
   */
  double StdDevB0() {
    return stddevb0;
  }

  /**
   * Standard deviation of the B1 parameter. 
   */
  double StdDevB1() {
    return stddevb1;

  }

  /**
   * Confidence interval of the B0 parameter at the
   * alpha-level of significance. This returns the positive
   * value of the width of half of the confidence interval.
   * Thus, the range is (B0 - CIB0 ... B0 + CIB0). Use p = 0.95
   * to compute the 95% confidence interval.
   */
  double CIB0 () {
    return cib0;
  }

  /**
   * Confidence interval of the B0 parameter at the
   * alpha-level of significance. This returns the positive
   * value of the width of half of the confidence interval.
   * Thus, the range is (B0 - CIB0 ... B0 + CIB0). Use p = 0.95
   * to compute the 95% confidence interval.
   */
  double CIB1 () {
    return cib1;
  }

  /**
   * Standard deviation for a prediction, assuming
   * 'm' samples are taken at that point. See the next function
   * for the limiting case of infinite observations.
   */
  double StdDevYmp(double xp, double m) {
    double tmp = (xp - mx);
    return se * sqrt(1/m + ninv + (tmp * tmp) / ssx_minus_nmxsq);
  }

  /**
   * Standard deviation for a prediction, assuming
   * infinite samples are taken at that point.
   */
  double StdDevYp(double xp) {
    double tmp = (xp - mx);
    return se * sqrt(ninv + (tmp * tmp) / ssx_minus_nmxsq);
  }

  /**
   * A prediction for a given point in the model
   */
  double yp(double xp) {
    return b0 + b1 * xp;
  }
  
  /**
   * The confidence interval for a given prediction, 'm' observations
   */
  double CIyp(double xp, double m) {
    return StdDevYmp(xp, m) * tdof;
  }

  /**
   * The confidence interval for a given prediction, inifinte observations
   */
  double CIyp(double xp) {
    return StdDevYp(xp) * tdof;
  }

  /**
   * Produce a printable report of the CUT_RegressionModel, showing
   * b0, b1 (and their confidence intervals) and se.
   */
  
  void out(ostream& str)
  {
  str << "[ b0 = (" << B0() << " +/- " << CIB0() << "), "
      << " b1 = (" << B1() << " +/- " << CIB1() << "), "
      << " se = " << StdDevErr() << ", "
      << " R = " << R() << ", "
      << " R2 = " << RSquared()
      << " ]";
  }

protected:
  /**
   * Data needed by the CUT_RegressionModel.
   */
  double n;
  double alpha;
  double dof;
  double tdof;
  double sxy;
  double mx;
  double sx;
  double ssx;
  double my;
  double sy;
  double ssy;
  double b1;
  double b0;
  double ss0;
  double sst;
  double sse;
  double ssr;
  double rsquared;
  double r;
  double mse;
  double se;
  double stddevb0;
  double stddevb1;
  double cib0;
  double cib1;
  double ssx_minus_nmxsq;
  double ninv;
};


#endif
