//-*-c++-*-
#ifndef _CUT_OptCacheSac_h_
#define _CUT_OptCacheSac_h_

#include "CUT_OptCache.h"

class OTS_HT;
class CUT_OptCacheSac : public CUT_OptCache {

    // degree of associativity
    int N;
    // max set field width
    int B;
    // min set field width
    int A;
    // Max no of addresses to be processed
    int T;

    // 2^N. i.e., Degree of associativity
    int TWO_PWR_N;
    int MAX_DEPTH;	// B-A, Number of range of sets simulated
    int TWO_POWER_MAX_DEPTH;	// 2^MAX_DEPTH
    // Masks
    int SET_MASK;
    int DIFF_SET_MASK;
    int SET_SIZE;
    int BASE_CACHE_SIZE;
    
    //
    // Hash table
    //
    OTS_HT **slot;
    //
    // Tags in the cashes
    //
    CUT_OptCache::Addr *tag_arr;
    //
    // Hit counts in caches
    //
    unsigned **hits;

    unsigned hit0;
    short *all_hit_flag;
    int *prty_arr;

    unsigned t_entries;
    unsigned unknowns;

  protected:
    virtual int stack_proc(int, int);
    virtual void gfsoptls (CUT_OptCache::Addr addr, int priority);
    virtual void inf_handler(CUT_OptCache::Addr addr, int cur_time);

    int inf_count;
    int Priority_sa(int i);
	
    void unk_hash_add_sa (CUT_OptCache::Addr addr, int prty);
    int unk_hash_del_sa (CUT_OptCache::Addr addr);
    void hash_clean_sa (CUT_OptCache::Addr addr, int max_prty);

  public:
    CUT_OptCacheSac(int L_ = 32,
		int N_ = 2,
		int A_ = 7, 
		int B_ = 10
		);
    virtual ~CUT_OptCacheSac();

    virtual void output(ostream&);
};

#endif
