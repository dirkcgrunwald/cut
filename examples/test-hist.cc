#include <iostream>
#include "CUT_Histogram.h"
#include <stdlib.h>

int
main()
{
  //
  //
  long howmany = 10000000;

  CUT_Histogram<long> data;

  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0xff;
    data += addr;
  }
    
  for (double d = 0; d < 1.0; d+= 0.10) {
    cout << d << "% quantile is " << data.quantile(d) << endl;
  }
  data.digestable_report(cout,"demo");
}

