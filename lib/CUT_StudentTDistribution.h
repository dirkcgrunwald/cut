//-*-c++-*-
#ifndef _CUT_StudentTDistribution_h_
#define _CUT_StudentTDistribution_h_

#include <math.h>
#include <values.h>
/**
 * The StudentT distribution
 */

class CUT_StudentTDistribution {

public:

  //
  // t-distribution: given p-value and degrees of freedom, return
  // t-value adapted from Peizer & Pratt JASA, vol63, p1416
  //

  static double student_t(long samples, double mean,
				 double stddev, double interval )
  {
    double samp = samples - 1;
    if ( samp < 0 ) return ( 0.0 );
    double t = tval((1.0+interval) * 0.5, samp);
    if (t == MAXDOUBLE) {
      return t;
    } else {
      return( (t * stddev) / sqrt((double) samp) );
    }
  }

  static double tval(double p, double degrees_of_freedom) {
    double t;
    double df = degrees_of_freedom;

    bool positive = p >= 0.5;

    if ( positive ) {
      p = 1.0 - p;
    }

    if (p <= 0.0 || df <= 0) {
      t = MAXDOUBLE;
    }
    else if (p == 0.5) {
      t = 0.0;
    }
    else if (df == 1) {
      t = 1.0 / tan((p + p) * 1.57079633);
    }
    else if (df == 2) {
      t = sqrt(1.0 / ((p + p) * (1.0 - p)) - 2.0);
    }
    else {	
	double a = sqrt(log(1.0 / (p * p)));
	double aa = a * a;
	a = a - ((2.515517 + (0.802853 * a) + (0.010328 * aa)) /
		 (1.0 + (1.432788 * a) + (0.189269 * aa) +
		  (0.001308 * aa * a)));
	t = df - 0.666666667 + 1.0 / (10.0 * df);
	t = sqrt(df * (exp(a * a * (df - 0.833333333) / (t * t)) - 1.0));
      }
    if ( positive ) {
      return t;
    } else {
      return -t;
    }
  }
};

#endif
