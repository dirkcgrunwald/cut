//-*-c++-*-
#ifndef _CUT_MTFV_h_
#define _CUT_MTFV_h_

//
// A move-to-front list implemented using a vector of fixed
// size. I.e., the list has a fixed number of items in it, and it
// always has that number of items. It's your job to determine if the
// items are valid or not by providing some method to discriminate
// valid/invalid entries in the instance type.
//
// This structure is intended to be used when implementing an
// associative data structure of medium associativity (4..64).  For
// less than four, you should consider a simple age-based mechanism
// with an unrolled loop. For more than 64, the sequential search here
// might be too slow.
//
// Primary Methods of interest:
//
//  bool find(T& what)
//
//  bool find_and_move_to_front(T& what)
//
//  void lru_replacement(T& what)


//
// max_entries are the number of entries in the structure. These
// entries are statically allocated.
//
// T is the type of the entry.
//
// Compare is the comparison object (see STL) we use compare objects
// of type T.
//
template<int max_entries, class T, class Compare>
class CUT_MTFV
{
  class entry {
  public:
    //
    // next & prev pointers are stored as int indicies into the
    // storage of a bank. This save space on an Alpha, and means an
    // 'entry' takes 1/2 a cache line for e.g., a TLB simulation.
    //
    short next;
    short prev;
    short valid;
    T contents;

    void unlink(entry* entries) {
      entries[next].prev = prev;
      entries[prev].next = next;
    }

    //
    // place 'me' before 'before' in the list, assuming
    // that I'm not already in the list
    //
    void link_before(entry* entries, int me, int before) {
      //
      // Fix myself
      //
      prev = entries[before].prev;
      next = before;

      //
      // now fix before (my next)
      //
      entries[before].prev = me;
      //
      // and my prev..
      //
      entries[prev].next = me;
    }

    //
    // Swap positions with another entry
    //
    void swap(entry* entries, int with) {
      short wp = entries[with].prev;
      short wn = entries[with].next;
      entries[with].prev = prev;
      entries[with].next = next;
      prev = wp;
      next = wn;
    }
  };

  short root;
  //
  // In practice, this has zero size..
  Compare compare;
  entry storage[max_entries];

public:
  CUT_MTFV(const T& initializer) {
    root = 0;
    storage[0].next = 0;
    storage[0].prev = 0;
    storage[0].contents = initializer;
    storage[0].valid = 0;
    //
    // Thread the free list of the storage
    //
    for (short j = 1; j < max_entries; j++ ) {
      storage[j].link_before(storage, j, root);
      storage[j].contents = initializer;
      storage[j].valid = 0;
    }
  }

  //
  // return TRUE if found in the list, FALSE otherwise
  //
  bool find(T& what) {
    short here = root;

    do {
      entry& h = storage[here];
      if ( h.valid & compare(h.contents, what) ) {
	return true;
      }
      here = storage[here].next;
    } while (here != root);
    return false;
  }

  //
  // return TRUE if found in the list, FALSE otherwise
  // As a side effect, move the referenced item to the front
  // of the list
  //

  bool find_and_move_to_front(T& what) {
    short here = root;

    do {
      entry& h = storage[here];
      if ( h.valid & compare(h.contents, what)) {
	if ( here != root ) {
	  storage[here].unlink(storage);
	  storage[here].link_before(storage, here, root);
	  root = here;
	}
	return true;
      }
      here = storage[here].next;
    } while (here != root);
    return false;
  }

  //
  // LRU replacement
  //
  void lru_replacement(T& what) {
    short lru = storage[root].prev;
    storage[lru].contents = what;
    storage[lru].valid = 1;
    root = lru;
  }

};

#endif
