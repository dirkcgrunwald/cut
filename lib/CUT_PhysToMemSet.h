//-*-c++-*-
#ifndef _CUT_PhysToMemSet_h_
#define _CUT_PhysToMemSet_h_

/////////////////////////////////////////////////////////////////////////////
// This object is used to define "regions" of memory and map them to
// "objects". We then define a set<> type to contain these regions.
// See the "test-rangemap.cc" example for a functioning example.
/////////////////////////////////////////////////////////////////////////////

#include <functional>
#include <stdint.h>
#include <set>

using namespace std;

template<class T = uint32_t>
class CUT_PhysToMemObj {
public:
  T low_;
  T high_;
public:
  CUT_PhysToMemObj(T low, T high)
  {
    low_ = low;
    high_ = high;
  }

  inline bool operator==(const CUT_PhysToMemObj& value) const
  {
    return (value.low_ >= low_ && value.high_ < high_);
  }

  inline bool operator<(const CUT_PhysToMemObj& value) const
  {
    return (value.high_ < low_);
  }
};

//
// This is the comparator function for the set - it's like less,
// but compares the things being pointed to
//
template<class T>
struct less_ptr : public binary_function<T*,T*,bool>
{
  bool operator ()(T* a_, T* b_) const {
    return ( *a_ < *b_ );
  }
};

//
// The actual datatype to instantiant looks like this.
// typedef set<RangeItem*, less_ptr<RangeItem>> RangeSet;
// RangeSet myset;
// 

#endif
