#include <iostream>
#include "CUT_RegressionData.h"
#include "CUT_RegressionModel.h"
#include <stdlib.h>

int
main()
{
  //
  //
  long howmany = 10000000;
  cout << "Testing regression model over  " << howmany << " samples\n";

  CUT_RegressionData<double> regress;

  for (int i = 0; i < howmany ; i++) {
    unsigned long x = ::random() % 0x7ff;
    unsigned long y = x + ::random() % 0xf;
    regress.add(x,y);
  }

  CUT_RegressionModel<double> model(regress,0.90);
  model.out(cout);
  cout << endl;
}

