#include <iostream>
#include "CUT_OptCacheSac.h"
#include <stdlib.h>

using namespace std;

int
main()
{
  CUT_OptCacheSac  cache;
  //
  // The optCache doesn't really use a cachdef, but we need to instantiate 
  //
  //
  long howmany = 100000;
  cout << "Insert " << howmany << " TLB references into a small 2-way associative TLB\n";

  for (int i = 0; i < howmany ; i++) {
    CUT_OptCache::Addr addr = ::random() % 0x7fff;
    cache.for_each_reference(addr);
  }
    
  cache.output(cout);
}

