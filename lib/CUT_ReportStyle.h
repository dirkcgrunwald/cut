//-*-c++-*-
#ifndef _CUT_ReportStyle_h_
#define _CUT_ReportStyle_h_

//
// A report style is used for reporting various quantities.
// The data that can be output can either be floating/double,
// string, int/long, char. The report style is charged with
// encapsulating the data structure used by the ReportStyle
// and providing the appropriate I/O routines.
//

class CUT_ReportSimpleScalar {
public:
  
};

#endif
