#include <iostream>
#include "CUT_Statistic.h"
#include <stdlib.h>

int
main()
{
  //
  //
  long howmany = 10000000;
  cout << "Insert " << howmany << " TLB references into a small 2-way associative TLB\n";

  CUT_Statistic<double> data;

  for (int i = 0; i < howmany ; i++) {
    unsigned long addr = ::random() % 0x7fff;
    data += addr;
  }
    
  data.digestable_report(cout,"demo");
}

