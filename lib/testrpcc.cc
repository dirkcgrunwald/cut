#include <stdio.h>
#include <unistd.h>
#include <math.h>

#ifndef __GNUC__
#include <c_asm.h>
#endif

inline unsigned long raw_rpcc()
{
#ifdef __GNUC__
  unsigned long rpcc;
  asm("rpcc %0" : "&r=" (rpcc));
#else
  rpcc = asm("rpcc %v0");
#endif
  unsigned long cycles = (rpcc & 0xffffffff);
  return cycles;
}

int
main()
{
  int rpcc = raw_rpcc();
//  fprintf(stderr,"Here's some cycles\n");
//  sin(0.48);

register int x = 0;

#define EAT x = x + 1;
#define X EAT EAT EAT EAT
#define Y  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X  X X X X X X X X 

//Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y Y 
    for (x = 0; x < 10000; x++);

  int rpcc2 = raw_rpcc();

  fprintf(stdout,"That was %d cycles to compute %d\n",
	  (int) (rpcc2 - rpcc), x);
  
  int rpcc3 = raw_rpcc();

  fprintf(stderr,"and %d cycles doing the printf\n",
	  (int) (rpcc3 - rpcc2));
}
