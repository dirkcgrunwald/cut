#include <iostream>
#include <stdlib.h>
#include "CUT_Knob.h"

Knob<int> one("one", "First value", 10);
Knob<string> two("two", "Second value", "second value");
Knob<long> three("three", "Third value", 100000L);
Knob<long long> four("four", "Fourth value", 100000LL);
Knob<char> five("five", "five value", 'x');
Knob<string> six("six", "six value", "This is a string");

int
main(int, char **)
{
  fprintf(stderr,"Here's the options\n");
  WriteKnobs(stderr);

  ReadKnob("one", "20");

  fprintf(stderr,"Here it is again!\n");
  WriteKnobs(stderr);

  FILE *f = fopen("/tmp/foo", "w");
  fprintf(f, "one -> 33\n");
  fprintf(f, "two -> this is the string\n");
  fprintf(f, "three -> 10000\n");
  fprintf(f, "four -> 10000\n");
  fprintf(f, "five -> y\n");
  fprintf(f, "six -> \"this is an option with trailing spaces\" \n");
  fclose(f);
  f = fopen("/tmp/foo", "r");

  ReadKnobs(f);
  fprintf(stderr,"Here it is one last time!\n");
  WriteKnobs(stderr);
}
