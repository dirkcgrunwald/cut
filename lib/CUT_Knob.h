//-*-c++-*-
#ifndef _CUT_Knob_
#define _CUT_Knob_

#include <map>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <string>

using namespace std;

class KnobBase;
typedef map<string, KnobBase*> KnobTableType;
extern KnobTableType _GlobalKnobTable;

class KnobBase {
  protected:
  char *_desc;

  KnobBase(string name, char *desc, KnobTableType& _table) {
    _desc = desc;
    _table[name] = this;
  }

  virtual ~KnobBase() {
    // Empty
  }

  public:  
  virtual void getKnob(ostringstream& str) = 0;
  virtual void setKnob(istringstream& str) = 0;
};

template <class T>
class Knob : public KnobBase {
private:
  T _value;
  public:

  Knob(string name, char *desc, T value,
       KnobTableType& _table = _GlobalKnobTable)
  : KnobBase(name, desc, _table)
  {
    _value = value;
  }

  ~Knob() {
    // Empty
  }

  T& value() {
    return _value;
  }

  T operator=(Knob<T>& knob)
  {
    return _value = knob.value();
  }

  operator T() {
    return value();
  }

  void getKnob(ostringstream & str)
  {
    str << _value;
  }

  void setKnob(istringstream & str)
  {
    str >> _value;
  }

};

//
// Functions that manipulate the global knob table
//
extern void WriteKnobs(FILE *file,
		       KnobTableType& _table = _GlobalKnobTable);

extern bool ReadKnob(char *name, char *value,
		     KnobTableType& _table = _GlobalKnobTable);

extern void ReadKnobs(FILE *file,
		      KnobTableType& _table = _GlobalKnobTable);

extern void ParseCmdKnobs(int argc, char **argv,
			  KnobTableType& _table = _GlobalKnobTable);
#endif
