#include <iostream>
#include <stdlib.h>

#include <assert.h>
#include <functional>

#define NEW 1

using namespace std;

#include "CUT_BinHeap.h"

void
foo()
{
  typedef less_equal<long> gtlong;
  typedef CUT_BinHeap<long, long, gtlong> C;


  for (int iterate = 0; iterate < 2; iterate++) {
    C heap;
    //
    // Start adding samples..
    //
    for (long samples = 0; samples < 1000; samples++) {
      long key = ::random() % 100000;
      heap.insert(key, samples);
    }
  
    cout << "There are " << heap.size() << " elements, height = ";
    cout << heap.tree_height() << endl;

    if ( iterate == 0) {
      cout << "Here are the first 10\n";
      for (int i = 0; i < 10; i++) {
	//
	// get the pseudo-index..
	//

	C::Pix pix = heap.find_top();
    
	//
	// tGet the data value..
	//
	long value = heap.top();
    
	cout << "(key,data) = " << heap.key(pix) << ",";
	cout << heap.data(pix) << ", or value = " << value << endl;
	//
	// Remove it..
	heap.deq();
      }
    }

    cout << "There are " << heap.size() << " elements, height = ";
    cout << heap.tree_height() << endl;

      C::Pix pix;
      int count = 0;
      for (pix = heap.first(); pix != NULL; heap.next(pix)) {
	if ( heap.data(pix)  == 43 ) {
	  cout << "Wow, found one...\n";
	  heap.erase(pix);
	  cout << "Now, there are now .." << heap.size() << " elements\n";
	}
	count++;
      }
      cout << "Looked at " << count << "items\n";

    //
    // Deq 50 items..
    //
    cout << "There are " << heap.size() << " elements, height = ";
    cout << heap.tree_height() << endl;

    for (int j = 0; j < 50; j++) {
      heap.deq();
    }
    cout << "There are " << heap.size() << " elements, height = ";
    cout << heap.tree_height() << endl;

    heap.clear();
    cout << "There are now " << heap.size() << " elements\n";
  }
  
}

int
main(int, char **)
{
  foo();
}
