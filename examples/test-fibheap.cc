#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <functional>
#include <utility>
#include "CUT_FibHeap.h"


void
foo()
{
  typedef greater<long> gtlong;
  typedef CUT_FibHeap<long, long, gtlong> C;
  C heap;

  for (int iterate = 0; iterate < 5; iterate++) {
    //
    // Start adding samples..
    //
    for (long samples = 0; samples < 1000; samples++) {
      long key = ::random();
      heap.insert(key, samples);
    }
  
    cout << "There are " << heap.size() << " elements\n";
    cout << "Here are the first 10\n";
    for (int i = 0; i < 10; i++) {
      //
      // get the pseudo-index..
      //
      C::Pix pix = heap.find_top();
    
      //
      // Get the data value..
      //
      long value = heap.top();
    
      cout << "(key,data) = " << heap.key(pix) << ",";
      cout << heap.data(pix) << ", or value = " << value << endl;
      //
      // Remove it..
      heap.deq();
    }

    cout << "There are now .." << heap.size() << " elements\n";
    if ( iterate == 2 ) {
      cout << "Looking for a 43...\n";
      C::Pix pix;
      int count = 0;
      for (pix = heap.first(); pix != NULL; heap.next(pix)) {
	if ( heap.data(pix)  == 43 ) {
	  cout << "Wow, found one...\n";
	}
	count++;
      }
      cout << "Looked at " << count << "items\n";
      heap.clear();
      cout << "There are now " << heap.size() << " elements\n";
    }
  }
  
}

int
main(int, char **)
{
  cout << "======================================================================\n";
  cout << " MinFibheap\n";
  cout << "======================================================================\n";
  foo();
}
